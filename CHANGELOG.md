# Changelog

## [0.2.1]
### Added
- An awesome logo

### Changed
- Rechristen the project to 'logos-clj'
- Some misc changes to the build script
- Some misc changes to the documentation

## [0.2.0]
### Added
- '@require' directive
- Support for variadic functions (varargs and varexprs)
- Every functionality of the core library now has an informative description + example in a comment
- Multiline input buffer in the REPL
- REPL commands: 'help' and 'abort'
- Customizable REPL initialization script
- Primitives: 'mod', 'to-int', 'rand'
- core.number: 'range', 'rand-int', 'even?', 'odd?'
- core.function: 'itself', 'apply', 'always', 'lpart', 'rpart', 'repeatedly'
- core.list: 'list', 'nth', 'take', 'filter', 'find', 'repeat'
- core.table (new module): 'table', 'tput', 'thas?', 'tget', 'tdel'
- core.formatting (new module): 'format-bool', 'format-int', 'format-string'
- core.parsing: 'atoi'
- core.stdio: 'printf', 'printfln'

### Changed
- Syntax for conditional expressions
- Syntax for lambda expressions
- REPL prefixes printed values with '='
- REPL formats lists more prettily
- JVM stack memory increased to mitigate the present lack of tail call optimization in Logos
- Primitives: 'readline' was renamed to 'readln'
- Primitives: 'add', 'sub', 'mul', 'div', 'eq?' and 'lt?' are now variadic
- core.boolean: 'and' and 'or' are now variadic
- core.boolean: 'and' and 'or' return truthy values instead of true
- core.number: 'gt?', 'lt-eq?' and 'gt-eq?' are now variadic
- core.function: 'compose' is now variadic
- core.list: 'concat' is now variadic
- core.string: 'join' is now variadic

## [0.1.0]
### Added
- A basic REPL
- Integer type
- Decimal type
- Cons pair type
- Keyword type
- String type
- Statements & blocks
- Name binding (including a syntax for lambda name binding)
- '%' expressions to refer to the value of the preceding statement
- Lambdas and closures
- 'self' keyword to enable anonymous lambdas to refer to themselves
- Conditionals
- Primitives: nil, add, sub, div, mul, pow eq?, lt?, cons, car, cdr, print & readline
- Comment syntax
- Syntax error detection, and (very barebones) evaluation error detection
- Launchers for Unix & Windows
- Interpreter directives to load Logos code from another file or from library
- Beginnings of a core library
