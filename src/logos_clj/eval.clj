(ns logos-clj.eval
  (:require [logos-clj.env :as env]
            [logos-clj.format :as fmt]))

(defn ->stmt
  [type & vals]
  (cons type vals))

(def ^{:private true} stmt-type
  first)

(def ^{:private true} stmt-vals
  rest)

(def logos-eval*)

(defmulti logos-eval ;; XXX (not sure about this one yet) make logos-eval methods return just the env instead of a pair consisting of the env and the value (and the refer to '%' in the environment to find its value)
  "Evaluates a Logos statement."
  (fn [env [type & vals]]
    type))

(def logos-eval*
  (comp second logos-eval))
