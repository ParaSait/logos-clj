(ns logos-clj.repl
  (:require [logos-clj.interpreter :as i]
            [logos-clj.logos-type :as lt]))

(def print* print)
(def println* println)

(def ^{:private true} decimal-format
  (java.text.DecimalFormat. "#.##########"))

(defn format-value
  [val]
  (letfn [(logos-list? [val]
            (and (list? val)
                 (or (nil? (second val))
                     (logos-list? (second val)))))
          (format-logos-list [ll]
            (str "("
                 ((fn f [ll*]
                    (if ll*
                      (str (format-value (first ll*))
                           (if (second ll*)
                             (str ", " (f (second ll*)))
                             ")"))))
                  ll)))]
    (cond
      (nil? val) "nil"
      (fn? val) "[lambda]"
      (double? val) (.format decimal-format val)
      (lt/logos-string? val) (str "\"" (lt/logos-string->clj-string val) "\"")
      (logos-list? val) (format-logos-list val)
      (list? val) (str "(" (format-value (first val)) " " (format-value (second val)) ")")
      :else val)))

(defn print-prompt!
  [input]
  (if-not input
    (print* "\n>>> ")
    (print* "  | "))
  (flush))

(defn print-result-value!
  [val]
  (println* (str "= " (format-value val))))

(defn print-help!
  []
  (println* "help  - print this message")
  (println* "exit  - exit the REPL")
  (println* "abort - clear the current multiline input buffer"))

(defn logos-repl
  [interpreter]
  (loop [input nil]
    (print-prompt! input)
    (let [line (read-line)]
      (case line
        "help" (do
                 (print-help!)
                 (recur input))
        "exit" (do
                 (println* "See ya!")
                 nil)
        "abort" (do
                  (println "Aborted.")
                  (recur nil))
        (let [input* (str input line "\n")
              next-input
              (try
                (-> input*
                    interpreter
                    print-result-value!)
                nil
                (catch clojure.lang.ExceptionInfo ex
                  (let [exdata (ex-data ex)]
                    (if-not (:incomplete exdata)
                      (binding [i/*error-printing-f* println*]
                        (i/handle-interpreter-exception exdata))
                      input*))))]
          (recur next-input))))))
