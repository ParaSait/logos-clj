(ns logos-clj.ast
  #_(:require))

(defmulti logos-analyze
  "Analyzes an AST to convert it to a statement format that's more efficient for the evaluator to process."
  (fn [[rule & nodes]]
    rule))
