(ns logos-clj.definition
  (:require [logos-clj.env :as env]
            [logos-clj.bindings :as bind]
            [logos-clj.format :as fmt]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]))

(defn- get-name
  [nodes]
  (-> nodes
      (nth 0)
      (nth 1)))

(defmethod ast/logos-analyze :basic-definition
  [[_ & nodes]]
  [:define
   (get-name nodes)
   (ast/logos-analyze (nth nodes 1))])

(defmethod ast/logos-analyze :lambda-definition
  [[_ & nodes]]
  [:define
   (get-name nodes)
   [:lambda
    (bind/analyze-binding-pattern (nth nodes 1))
    (ast/logos-analyze (nth nodes 2))]])

(defmethod eval/logos-eval :define
  [env [_ & vals]]
  (let [new-binding (env/->binding (first vals) (eval/logos-eval* env (second vals)))
        prev-binding (env/put-local-binding! env new-binding)]
    [env
     (fmt/repl-event (str (if prev-binding "redefined " "defined ") (fmt/single-quote (first vals))))]))
