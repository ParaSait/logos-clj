(ns logos-clj.bindings
  (:require [logos-clj.env :as env]))

(defn- as-logos-list
  [cl]
  (when-not (empty? cl)
    (list (first cl) (as-logos-list (rest cl)))))

(def analyze-binding-pattern
  rest)

(defn bind-values
  [binding-pattern binding-values]
  (cond
    (and (empty? binding-pattern) (empty? binding-values)) (list)
    :else (let [[type name] (first binding-pattern)]
            (case type
              :arg (if-not (empty? binding-values)
                     (cons (env/->binding name (first binding-values))
                           (bind-values (rest binding-pattern) (rest binding-values)))
                     (throw (ex-info "binding error" {}))) ;; XXX flesh this out when adding semantic error checking
              :vararg (list (env/->binding name (as-logos-list binding-values)))))))
