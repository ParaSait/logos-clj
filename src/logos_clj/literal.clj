(ns logos-clj.literal
  (:require [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.logos-type :as lt]))

(defmethod ast/logos-analyze :int-literal
  [[_ & nodes]]
  [:literal (Integer/parseInt (nth nodes 0))])

(defmethod ast/logos-analyze :decimal-literal
  [[_ & nodes]]
  [:literal (Double/parseDouble (nth nodes 0))])

(defmethod ast/logos-analyze :keyword-literal
  [[_ & nodes]]
  [:literal (keyword (nth nodes 0))])

(defmethod ast/logos-analyze :string-literal
  [[_ & nodes]]
  [:literal (lt/clj-string->logos-string (nth nodes 0))])

(defmethod eval/logos-eval :literal
  [env [_ & vals]]
  [env
   (first vals)])
