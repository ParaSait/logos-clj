(ns logos-clj.format
  #_(:require))

(defn- surround-by
  [surr s]
  (str surr s surr))

(defn repl-event
  [s]
  (surround-by "*" s))

(defn single-quote
  [s]
  (surround-by "'" s))
