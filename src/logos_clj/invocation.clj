(ns logos-clj.invocation
  (:require [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.logos-type :as lt]))

(defn- as-clj-list
  [ll]
  (if ll
    (cons (first ll) (as-clj-list (second ll)))
    (list)))

(defmethod ast/logos-analyze :invocation
  [[_ & nodes]]
  (eval/->stmt :invoke
               (ast/logos-analyze (first nodes))
               (map ast/logos-analyze (rest nodes))))

(defmethod ast/logos-analyze :varexpr
  [[_ & nodes]]
  (eval/->stmt :varexpr
               (ast/logos-analyze (first nodes))))

(defn- eval-arg-exprs
  [env arg-exprs]
  (loop [arg-exprs arg-exprs
         args []]
    (if (not-empty arg-exprs)
      (let [arg-expr (first arg-exprs)]
        (if-not (= (first arg-expr) :varexpr)
          (recur (rest arg-exprs)
                 (conj args
                       (second (eval/logos-eval env arg-expr))))
          (recur (rest arg-exprs)
                 (concat args
                         (as-clj-list (second (eval/logos-eval env (second arg-expr))))))))
      args)))

(defmethod eval/logos-eval :invoke
  [env [_ & vals]]
  (let [f (second (eval/logos-eval env (first vals)))
        args (eval-arg-exprs env (second vals))]
    [env (f args)]))
