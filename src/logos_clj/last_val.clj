(ns logos-clj.last-val
  (:require [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]))

(defmethod ast/logos-analyze :last-val ;; XXX give proper names to the selectors
  [[_ & nodes]]
  [:last-val])

(defmethod eval/logos-eval :last-val
  [env [_ & vals]]
  [env
   (env/get-last-val env)])
