(ns logos-clj.methods
  (:require [logos-clj.ast]
            [logos-clj.eval]
            [logos-clj.statement]
            [logos-clj.last-val]
            [logos-clj.literal]
            [logos-clj.identifier]
            [logos-clj.definition]
            [logos-clj.invocation]
            [logos-clj.lambda]
            [logos-clj.conditional]
            [logos-clj.directive]))
