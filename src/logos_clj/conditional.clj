(ns logos-clj.conditional
  (:require [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]))

(defmethod ast/logos-analyze :conditional
  [[_ & nodes]]
  (letfn [(analyze-clauses [clauses]
            (let [first-clause (first clauses)
                  condition (nth first-clause 1)
                  consequent (nth first-clause 2)
                  rest-clauses (rest clauses)
                  result [:conditional
                          (ast/logos-analyze condition)
                          (ast/logos-analyze consequent)]]
              (if (not-empty rest-clauses)
                (conj result (analyze-clauses rest-clauses))
                result)))]
    (analyze-clauses nodes)))

(def ^{:private true} is-truthy?
  (complement nil?))

(defmethod eval/logos-eval :conditional
  [env [_ & vals]]
  (let [[_ val] (eval/logos-eval env (nth vals 0))]
    (if (is-truthy? val)
      (eval/logos-eval env (nth vals 1))
      (if (> (count vals) 2)
        (eval/logos-eval env (nth vals 2))
        [env nil]))))
