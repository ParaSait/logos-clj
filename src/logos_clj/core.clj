(ns logos-clj.core
  (:gen-class)
  (:require [clojure.string :refer [blank?]]
            [logos-clj.interpreter :as i]
            [logos-clj.loader :as l]
            [logos-clj.repl :as repl]))

(defn- reload
  []
  (use 'logos-clj.core :reload))

(defn parse-args
  [args]
  (loop [args* args
         state :root
         parsed-args {}]
    (let [next-arg (first args*)]
      (case state
        :root (case next-arg
                "-i" (recur (rest args*)
                            :root
                            (assoc parsed-args :interactive true))
                "-f" (recur (rest args*)
                            :file-path
                            parsed-args)
                nil parsed-args
                :invalid)
        :file-path (case next-arg
                     nil :invalid
                     (recur (rest args*)
                            :root
                            (assoc parsed-args :file-path next-arg)))))))

(def args-valid?
  (partial not= :invalid))

(def interactive-mode?
  :interactive)

(def get-file-path
  :file-path)

(defn- load-from-file!
  [loader-settings interpreter file-path]
  (when file-path
    (try
      (l/load-code-from-file-path! loader-settings interpreter file-path) ;; XXX handle i/o exceptions
      (catch clojure.lang.ExceptionInfo ex
        (binding [*out* *err*
                  i/*error-printing-f* println]
          (i/handle-interpreter-exception (ex-data ex))))))) ;; XXX include filename in the error message

(defn- print-help-message!
  []
  (println "Invalid parameters! Please refer to the manual for help."))

(defn- determine-exit-code
  [exit-value]
  (if (integer? exit-value)
    exit-value
    1))

(defn- get-logos-base-path
  []
  (let [path (System/getenv "LOGOS_BASE_PATH")]
    (if (blank? path)
      "."
      path)))

(defn- get-logos-lib-path
  []
  (let [path (System/getenv "LOGOS_LIB_PATH")]
    (if (blank? path)
      "./lib"
      path)))

(defn- get-repl-init-file-path
  []
  (str (get-logos-base-path) "/repl-init.lgs"))

(defn- file-exists?
  [path]
  (.exists (clojure.java.io/as-file path)))

(defn- load-repl-init-file-if-present!
  [loader-settings interpreter]
  (let [repl-init-file-path (get-repl-init-file-path)]
    (when (file-exists? repl-init-file-path)
      (load-from-file! loader-settings interpreter repl-init-file-path))))

(defn -main
  [& args]
  (let [parsed-args (parse-args args)]
    (if (args-valid? parsed-args)
      (let [loader-settings (l/->loader-settings (get-logos-lib-path))
            interpreter (i/->interpreter loader-settings)]
        (let [exit-value (let [file-exit-code (load-from-file! loader-settings interpreter (get-file-path parsed-args))]
                           (if (interactive-mode? parsed-args)
                             (do
                               (load-repl-init-file-if-present! loader-settings interpreter)
                               (repl/logos-repl interpreter)
                               0)
                             file-exit-code))]
          (determine-exit-code exit-value)))
      (do
        (print-help-message!)
        1))))
