(ns logos-clj.lambda
  (:require [logos-clj.env :as env]
            [logos-clj.bindings :as bind]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]))

(defmethod ast/logos-analyze :lambda
  [[_ & nodes]]
  [:lambda (bind/analyze-binding-pattern (first nodes))
           (ast/logos-analyze (second nodes))])

(defmethod eval/logos-eval :lambda
  [env [_ & vals]]
  (let [args-pattern (first vals)
        body (second vals)]
    (letfn [(l [args]
              (as-> (bind/bind-values args-pattern args) x
                (conj (vec x) (env/->binding "self" l))
                (env/push-frame env x)
                (eval/logos-eval x body)
                (second x)))]
      [env l])))
