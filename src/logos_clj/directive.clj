(ns logos-clj.directive
  (:require [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]))

(defmethod ast/logos-analyze :load-file
  [[_ & nodes]]
  [:load-file (nth (nth nodes 0) 1)])

(defmethod ast/logos-analyze :load-lib
  [[_ & nodes]]
  [:load-lib (rest (nth nodes 0))])

(defmethod ast/logos-analyze :require-file
  [[_ & nodes]]
  [:require-file (nth (nth nodes 0) 1)])

(defmethod ast/logos-analyze :require-lib
  [[_ & nodes]]
  [:require-lib (rest (nth nodes 0))])
