(ns logos-clj.env ;; XXX optimize underlying representation of frames by using maps?
  #_(:require))

(defn ->binding
  [name val]
  [name val])

(def ^{:private true} binding-name
  first)

(def ^{:private true} binding-val
  second)

(defn- lookup-binding-in-frame
  [frame name]
  (->> @frame
       (filter #(= (binding-name %) name))
       (first)))

(defn- lookup-binding-in-frame-list
  [frames name]
  (->> frames
       (map #(lookup-binding-in-frame % name))
       (filter identity)
       (first)))

(defn ->empty-env
  []
  {:frames (list)
   :last nil})

(defn push-frame
  ([env frame]
   (update env :frames (partial cons (atom frame))))
  ([env]
   (push-frame env [])))

(def ^{:private true} local-frame
  (comp first :frames))

(defn put-local-binding!
  [env new-binding]
  (let [frame (local-frame env)
        prev-binding (lookup-binding-in-frame frame (binding-name new-binding))]
    (swap! frame (partial cons new-binding))
    prev-binding))

(defn env-lookup
  [env name]
  (when-let [binding (lookup-binding-in-frame-list (:frames env) name)]
    (binding-val binding)))

(defn set-last-val
  [env val]
  (assoc env :last val))

(defn get-last-val
  [env]
  (get env :last))
