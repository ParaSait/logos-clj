(ns logos-clj.statement
  (:require [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]))

(defmethod ast/logos-analyze :command-list
  [[_ & nodes]]
  (case (count nodes)
    0 [:noop]
    1 (ast/logos-analyze (first nodes))
    [:command-pair
     (ast/logos-analyze (first nodes))
     (ast/logos-analyze (cons :command-list (rest nodes)))]))

(defmethod ast/logos-analyze :statement-list
  [[_ & nodes]]
  (case (count nodes)
    0 [:noop]
    1 (ast/logos-analyze (first nodes))
    [:statement-pair
     (ast/logos-analyze (first nodes))
     (ast/logos-analyze (cons :statement-list (rest nodes)))]))

(defmethod ast/logos-analyze :statement
  [[_ & nodes]]
  [:statement (ast/logos-analyze (first nodes))])

(defmethod eval/logos-eval :noop
  [env [_ & vals]]
  [env nil])

(defmethod eval/logos-eval :statement
  [env [_ & vals]]
  (let [[env* val] (eval/logos-eval env (first vals))]
    [(env/set-last-val env* val)
     val]))

(defmethod eval/logos-eval :command-pair
  [env [_ & vals]]
  (let [[env* _] (eval/logos-eval env (first vals))]
    (eval/logos-eval env* (second vals))))

(defmethod eval/logos-eval :statement-pair
  [env [_ & vals]]
  (let [[env* _] (eval/logos-eval env (first vals))]
    (eval/logos-eval env* (second vals))))
