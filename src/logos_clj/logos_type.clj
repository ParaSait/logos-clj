(ns logos-clj.logos-type
  #_(:require))

(defn- clj-list->logos-list ;; XXX optimize
  [l]
  (loop [clj-list (reverse l)
         logos-list nil]
    (if (first clj-list)
      (recur (rest clj-list)
             (list (first clj-list) logos-list))
      logos-list)))

(defn- logos-list->clj-list ;; XXX optimize
  [l]
  (loop [logos-list l
         clj-list (list)]
    (if (list? logos-list)
      (recur (second logos-list)
             (cons (first logos-list) clj-list))
      (reverse clj-list))))

(defn logos-string?
  [val]
  (and (list? val)
       (= (first val) :string)))

(defn clj-string->logos-string
  [cs]
  (->> cs
       (seq)
       (map int)
       (cons :string)
       (clj-list->logos-list)))

(defn logos-string->clj-string
  [ls]
  (->> ls
       (logos-list->clj-list)
       (rest)
       (map char)
       (apply str)))
