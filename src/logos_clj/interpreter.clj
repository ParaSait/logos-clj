(ns logos-clj.interpreter
  (:require [clojure.string :refer [join]]
            [logos-clj.env :as env]
            [logos-clj.parser :as p]
            [logos-clj.ast :as a]
            [logos-clj.eval :as e]
            [logos-clj.logos-type :as lt]
            [logos-clj.loader :as l]
            [logos-clj.methods]))

(def ^:dynamic *handle-eval-errors*
  true)

(def ^:dynamic *error-printing-f*
  println)

(defn print+flush
  [s]
  (print s)
  (flush))

(def rand*
  rand)

(def ^{:private true} primitives
  {"nil" nil
   "add" (fn [args]
           (apply + args))
   "sub" (fn [[a b & rest]]
           (apply - a b rest))
   "mul" (fn [args]
           (apply * args))
   "div" (fn [[a b & rest]]
           (double (apply / a b rest)))
   "mod" (fn [[a b]]
           (rem a b))
   "pow" (fn [[a b]]
           (Math/pow a b))
   "to-int" (fn [[n]]
              (int n))
   "eq?" (fn [[a b & rest]]
           (when (apply = a b rest)
             0))
   "lt?" (fn [[a b & rest]]
           (when (apply < a b rest)
             0))
   "cons" (fn [[a b]]
            (list a b))
   "car" (fn [[p]]
           (first p))
   "cdr" (fn [[p]]
           (second p))
   "rand" (fn [_]
            (rand*))
   "print" (fn [[s]]
             (print+flush (lt/logos-string->clj-string s))
             nil)
   "readln" (fn [_]
              (lt/clj-string->logos-string (read-line)))})

(defn- ->root-env
  []
  (-> (env/->empty-env)
      (env/push-frame (map (fn [[k v]]
                             (env/->binding k v))
                           primitives))))

(defn- process! ;; XXX this could use some cleanup
  [i state cmd loader-settings]
  (let [type (first cmd)]
    (case type
      :command-pair (do
                      (process! i state (nth cmd 1) loader-settings)
                      (process! i state (nth cmd 2) loader-settings))
      :load-file (do
                   (l/load-code-from-file-path! loader-settings i (second cmd))
                   (str "*loaded '" (second cmd) "'*"))
      :load-lib (do
                  (l/load-code-from-lib-spec! loader-settings i (second cmd))
                  (str "*loaded '" (join "." (second cmd)) "'*"))
      :require-file (if-not (some #{(second cmd)} (:required @state))
                      (do
                        (l/load-code-from-file-path! loader-settings i (second cmd))
                        (swap! state (fn [st] (update st :required (fn [rq] (conj rq (second cmd))))))
                        (str "*loaded '" (second cmd) "'*"))
                      (str "*already loaded '" (second cmd) "'*"))
      :require-lib (if-not (some #{(second cmd)} (:required @state))
                     (do
                       (l/load-code-from-lib-spec! loader-settings i (second cmd))
                       (swap! state (fn [st] (update st :required (fn [rq] (conj rq (second cmd))))))
                       (str "*loaded '" (join "." (second cmd)) "'*"))
                     (str "*already loaded '" (join "." (second cmd)) "'*"))
      (let [[env val] (e/logos-eval (:env @state) cmd)]
        (swap! state (fn [st] (assoc st :env env)))
        val))))

(defn ->interpreter
  [loader-settings]
  (let [state (atom {:env (->root-env)
                     :required (list)})]
    (fn i [code]
      (let [parse-result (p/logos-parse code)]
        (if-not (p/failure? parse-result)
          (try
            (process! i state (a/logos-analyze parse-result) loader-settings)
            (catch Exception ex
              (if *handle-eval-errors*
                (throw (ex-info "Logos evaluation error"
                                {:type :logos-eval-error}))
                (throw ex))))
          (throw (ex-info "Logos syntax error"
                          {:type :logos-syntax-error
                           :text (p/failure-text parse-result)
                           :line (p/failure-line parse-result)
                           :column (p/failure-column parse-result)
                           :incomplete (p/incomplete? parse-result code)})))))))

(defn format-syntax-error
  [ex-data]
  (str "Syntax error on line " (:line ex-data) ", column " (:column ex-data) ":\n"
       (:text ex-data) "\n"
       (apply str (repeat (dec (:column ex-data)) " ")) "^"))

(defn- print-syntax-error
  [ex-data]
  (*error-printing-f* (format-syntax-error ex-data)))

(defn- print-eval-error
  [ex-data]
  (*error-printing-f* "Evaluation error."))

(defn handle-interpreter-exception
  [ex-data]
  (case (:type ex-data)
    :logos-syntax-error (print-syntax-error ex-data)
    :logos-eval-error (print-eval-error ex-data)))
