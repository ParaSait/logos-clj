(ns logos-clj.identifier
  (:require [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.env :as env]))

(defmethod ast/logos-analyze :identifier ;; XXX give proper names to the selectors
  [[_ & nodes]]
  [:identifier (nth nodes 0)])

(defmethod eval/logos-eval :identifier ;; XXX rename to 'reference'?
  [env [_ & vals]]
  [env
   (env/env-lookup env (first vals))])
