(ns logos-clj.parser
  (:require [instaparse.core :as insta]))

(def ^{:private true} parser
  (insta/parser
   "<code> = command-list
    command-list = <ws>* (command <ws>+)* command?
    <command> = directive | statement
    <directive> = load-directive | require-directive
    <load-directive> = load-file | load-lib
    <require-directive> = require-file | require-lib
    load-file = <'@load'> <ws>+ string-literal
    load-lib = <'@load'> <ws>+ lib-spec
    require-file = <'@require'> <ws>+ string-literal
    require-lib = <'@require'> <ws>+ lib-spec
    lib-spec = lib-spec-segment (<'.'> lib-spec-segment)*
    <lib-spec-segment> = #'[a-zA-Z0-9\\-_]+'
    statement-list = <ws>* (statement <ws>+)* statement?
    statement = expression | definition
    <expression> = last-val | literal | identifier | invocation | lambda | conditional | list-expr
    last-val = <'%'>
    <literal> = int-literal | decimal-literal | keyword-literal | string-literal
    int-literal = #'-?[0-9]+'
    decimal-literal = #'-?[0-9]+\\.[0-9]+'
    keyword-literal = <':'> #'[0-9a-zA-Z\\-_\\+\\*$\\?!]+'
    string-literal = <'\"'> #'[^\"]*' <'\"'>
    <identifier-token> = #'[a-zA-Z_\\+\\*$\\?!][0-9a-zA-Z\\-_\\+\\*$\\?!]*'
    identifier = identifier-token
    invocation = expression <ws>* <'('> <ws>* invocation-arglist <ws>* <')'> <ws>*
    <invocation-arglist> = expression <ws>* <','> <ws>* invocation-arglist | expression | varexpr | epsilon
    varexpr = expression <'...'>
    lambda = <'{'> <ws>* <'|'> <ws>* binding-pattern <ws>* <'|'> <ws>* statement-list <ws>* <'}'>
    binding-pattern = binding-pattern-1
    <binding-pattern-1> = arg <ws>* <','> <ws>* binding-pattern-1 | arg | vararg | epsilon
    arg = identifier-token
    vararg = identifier-token <'...'>
    conditional = <'['> <ws>* conditional-clause+ <ws>* <']'>
    conditional-clause = expression <ws>* <':'> <ws>+ expression <ws>* <','>? <ws>*
    <list-expr> = <'{'> <ws>* statement-list <ws>* <'}'>
    <definition> = basic-definition | lambda-definition
    basic-definition = identifier <ws>* <'<-'> <ws>* expression
    lambda-definition = identifier <'('> <ws>* binding-pattern <ws>* <')'> <ws>* <'<-'> <ws>* expression
    ws = <#'( |\\r|\\n|\\t)+'> | <comment>
    comment = #'#.*\\n?'"))

(defn logos-parse
  "Parses a string of Logos code into a list of ASTs."
  [s]
  (let [r (parser s)]
    (if-not (insta/failure? r)
      (first r)
      r)))

(def failure?
  insta/failure?)

(defn incomplete?
  "Determines if a result r is a failure and whether the failure is due to it being incomplete rather than due to something being wrong. Requires the original string s to figure it out."
  [r s]
  (and (failure? r)
       (= (:index (insta/get-failure r))
          (count s))))

(def failure-text
  :text)

(def failure-line
  :line)

(def failure-column
  :column)
