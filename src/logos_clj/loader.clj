(ns logos-clj.loader
  (:require [clojure.string :refer [join blank?]]))

(defn ->loader-settings
  [lib-path]
  {:lib-path lib-path})

(defn load-code-from-file-path!
  [loader-settings interpreter file-path]
  (interpreter (slurp file-path))) ;; XXX handle i/o exceptions

(defn resolve-lib-spec
  [loader-settings lib-spec]
  (let [lib-path (:lib-path loader-settings)]
    (if-not (blank? lib-path)
      (str (join "/" [lib-path (join "/" lib-spec)]) ".lgs")
      (throw (ex-info "lib path is empty!" {})))))

(defn load-code-from-lib-spec!
  [loader-settings interpreter lib-spec]
  (load-code-from-file-path! loader-settings
                             interpreter
                             (resolve-lib-spec loader-settings lib-spec)))
