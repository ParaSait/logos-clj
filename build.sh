#!/bin/bash
echo "Setting environment..."
source setenv.sh

archive_filename="logos-${LOGOS_BUILD_VERSION}-bin.tar.gz"

if [[ -d build ]]; then
  echo "Deleting previous build directory..."
  rm -r build
fi

if [[ -f $archive_filename ]]; then
  echo "Deleting previous archive..."
  rm $archive_filename
fi

echo "Creating build directory..."
mkdir build

echo "Building uberjar..."
lein uberjar

echo "Adding uberjar..."
mkdir build/bin
cp target/logos-clj-${LOGOS_BUILD_VERSION}-standalone.jar build/bin

echo "Adding lib..."
cp -R lib build/lib

echo "Creating launcher for Unix..."
cat <<EOF >build/logos
#!/bin/sh
export LOGOS_BASE_PATH="\$( cd "\$( dirname "\${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export LOGOS_LIB_PATH="\$LOGOS_BASE_PATH/lib"
java -Xss10M -jar bin/logos-clj-${LOGOS_BUILD_VERSION}-standalone.jar \$@
EOF
chmod +x build/logos

echo "Creating launcher for Windows..."
cat <<EOF >build/logos.bat
#!/bin/sh
@echo off
set LOGOS_BASE_PATH=%~dp0
set LOGOS_LIB_PATH=%LOGOS_BASE_PATH%\lib
java -Xss10M -jar %~dp0\bin\logos-clj-${LOGOS_BUILD_VERSION}-standalone.jar %*
EOF

echo "Adding cool logo..."
cp logo.png build

echo "Adding documentation..."
cp README.md build
cp CHANGELOG.md build

echo "Adding the repl-init.lgs file..."
cp misc/repl-init.lgs build

echo "Adding license..."
cp LICENSE build

echo "Creating an archive..."
cd build
tar -czvf $archive_filename ./*
mv $archive_filename ..
cd ..

echo "Done!"
