@require core.lang
@require core.boolean

# inc(n)
# Increments a number. Ex:
#   inc(2) = 3
inc(n) <- add(n, 1)

# dec(n)
# Decrements a number. Ex:
#   dec(3) = 2
dec(n) <- sub(n, 1)

# negate(n)
# Negates a number. Ex:
#   negate(5) = -5
#   negate(-5) = 5
negate(n) <- sub(0, n)

# invert(n)
# Inverts a number. Ex:
#   invert(5) = 0.2
#   invert(0.2) = 5.0
invert(n) <- div(1, n)

# even?(n)
# Tests if a number is even. Ex:
#   even?(2) = true
#   even?(3) = false
even?(n) <- {
  to-int(n)
  mod(%, 2)
  eq?(%, 0)
}

# odd?(n)
# Tests if a number is odd. Ex:
#   even?(2) = false
#   even?(3) = true
odd?(n) <- {
  to-int(n)
  mod(%, 2)
  eq?(%, 1)
}

# gt?(a, b, rest...)
# Tests if each argument is greater than the next. Ex:
#   gt?(3, 2) = true
#   gt?(2, 3) = false
#   gt?(2, 2) = false
#   gt?(3, 2, 1) = true
#   gt?(3, 1, 2) = false
gt?(a, b, rest...) <- {
  and(not(eq?(a, b)),
      not(lt?(a, b)))
  and(%, [rest : self(b, rest...)
          else : 0])
}

# lt-eq?(a, b, rest...)
# Tests if each argument is less than or equal to the next. Ex:
#   lt-eq?(2, 3) = true
#   lt-eq?(3, 2) = false
#   lt-eq?(2, 2) = true
#   lt-eq?(1, 2, 2, 3) = true
#   lt-eq?(1, 2, 1, 1) = false
lt-eq?(a, b, rest...) <- {
  or(eq?(a, b),
     lt?(a, b))
  and(%, [rest : self(b, rest...)
          else : 0])
}

# gt-eq?(a, b, rest...)
# Tests if each argument is less than or equal to the next. Ex:
#   gt-eq?(3, 2) = true
#   gt-eq?(2, 3) = false
#   gt-eq?(2, 2) = true
#   gt-eq?(3, 2, 2, 1) = true
#   gt-eq?(3, 2, 3, 3) = false
gt-eq?(a, b, rest...) <- {
  or(eq?(a, b),
     not(lt?(a, b)))
  and(%, [rest : self(b, rest...)
          else : 0])
}

# abs(n)
# Returns the absolute value of a number.
#   abs(5) = 5
#   abs(-5) = 5
abs(n) <-
  [lt?(n, 0) : negate(n),
   else      : n]

# range(from, to)
# Returns the range of integers within the given bounds (exclusive!). Ex:
#   range(3, 8) = list(3, 4, 5, 6, 7)
range(from, to) <-
  [lt?(from, to) : cons(from, range(inc(from), to))
   gt?(from, to) : cons(from, range(dec(from), to))]

# rand-int(max)
# Prays to the RNG gods for a uniformly distributed random integer between 0 and the specified maximum (exclusive!). Ex:
#   rand-int(10) ~= some n such that: find(range(0, 10), n)
rand-int(max) <- {
  rand()
  mul(%, max)
  to-int(%)
}
