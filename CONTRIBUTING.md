# Contributing

I treat this as mostly a solo project (for now). But issues or small merge requests are always welcome.

I especially welcome example programs! Not only are they fun to collect, but they also show Logos in action, and they give me a perspective on its usage patterns in practice.

At a later stage, you could contribute by fleshing out the non-core part of the (to-be-)standard library. It's not really a "standard" library yet, but I might make a standard out of it later. However, it's best to wait with that until I've pinned down the core essentials of the language.

A new logo is welcome too! :D
