(defproject logos-clj "0.2.1"
  :description "An interpreter for a prototype of the Logos programming language."
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [instaparse "1.4.10"]]
  :profiles {:dev {:dependencies [[midje "1.9.9"]]}}
  :main logos-clj.core
  :aot [logos-clj.core]
  :repl-options {:init-ns logos-clj.core})
