(ns logos-clj.interpreter-test
  (:require [midje.sweet :refer :all]
            [logos-clj.interpreter :as i]
            [logos-clj.loader :as l]
            [logos-clj.logos-type :as lt]))

(def ^{:private true} test-loader-settings
  (l/->loader-settings "some-lib-dir"))

(defn- setup-interpreter
  []
  (binding [i/*handle-eval-errors* false] ;; FIXME this rebind is not working
    (i/->interpreter test-loader-settings)))

(facts "about the interpreter itself"
       (fact "it's a stateful Logos machine"
             (let [i (setup-interpreter)]
               (i "foo <- 123")
               (i "foo") => 123))

       (fact "throws exceptions containing text, line, column and incompleteness flag on syntax error"
             (let [i (setup-interpreter)]
               (i "abc <- 123\ndef <- <- 456\nghi <- 789")
               => (throws clojure.lang.ExceptionInfo
                          (fn [ex]
                            (let [data (ex-data ex)]
                              (and (= (:type data) :logos-syntax-error)
                                   (= (:text data) "def <- <- 456")
                                   (= (:line data) 2)
                                   (= (:column data) 8)
                                   (contains? data :incomplete)
                                   (not (:incomplete data))))))))
       
       (fact "on a syntax error, the incompleteness flag is truthy if the code can be completed"
             (let [i (setup-interpreter)]
               (i "abc <- 123\ndef <-")
               => (throws clojure.lang.ExceptionInfo
                          (fn [ex]
                            (let [data (ex-data ex)]
                              (and (= (:type data) :logos-syntax-error)
                                   (= (:text data) "def <-")
                                   (= (:line data) 2)
                                   (= (:column data) 7)
                                   (contains? data :incomplete)
                                   (:incomplete data))))))))

(facts "about format-syntax-error"
       (fact "formats a syntax error"
             (i/format-syntax-error
              {:type :logos-syntax-error
               :text "def <- <- 456"
               :line 2
               :column 8
               :incomplete false})
             => (str "Syntax error on line 2, column 8:\n"
                     "def <- <- 456\n"
                     "       ^")))

(facts "about the interpretation of Logos code"
       (fact "interprets int literals"
             (let [i (setup-interpreter)]
               (i "123") => 123))

       (fact "int literals can be negative if they're prefixed by a minus sign"
             (let [i (setup-interpreter)]
               (i "-123") => -123))

       (fact "interprets keyword literals"
             (let [i (setup-interpreter)]
               (i ":foo") => :foo))

       (fact "interprets string literals"
             (let [i (setup-interpreter)]
               (let [val (i "\"foo\"")]
                 (lt/logos-string? val) => truthy
                 (lt/logos-string->clj-string val) => "foo")))

       (fact "interprets decimal literals (as double values)"
             (let [i (setup-interpreter)]
               (i "12.34") => 12.34))

       (fact "interprets negative decimal literals (as negative double values)"
             (let [i (setup-interpreter)]
               (i "-12.34") => -12.34))

       (fact "interprets undefined references as nil"
             (let [i (setup-interpreter)]
               (i "foo") => nil?))

       (fact "interprets defined references as the value they refer to"
             (let [i (setup-interpreter)]
               (i "foo <- 123")
               (i "foo") => 123))

       (fact "definitions can override each other"
             (let [i (setup-interpreter)]
               (i "foo <- 123")
               (i "foo") => 123
               (i "foo <- 456")
               (i "foo") => 456))

       (fact "the special keyword 'nil' evaluates to nil"
             (let [i (setup-interpreter)]
               (i "nil") => nil?))
       
       (fact "the 'add' function can be used to add two numbers"
             (let [i (setup-interpreter)]
               (i "add()") => 0
               (i "add(2)") => 2
               (i "add(2, 3)") => 5
               (i "add(2, 3, 4)") => 9))

       (fact "the 'sub' function can be used to sub two numbers"
             (let [i (setup-interpreter)]
               (i "sub(2, 3)") => -1
               (i "sub(2, 3, 4)") => -5))

       (fact "the 'mul' function can be used to mul two numbers"
             (let [i (setup-interpreter)]
               (i "mul()") => 1
               (i "mul(3)") => 3
               (i "mul(3, 4)") => 12
               (i "mul(3, 4, 5)") => 60))

       (fact "the 'div' function can be used to div two numbers"
             (let [i (setup-interpreter)]
               (i "div(2, 3)") => (every-checker
                                   double?
                                   (roughly 0.666666666))
               (i "div(2, 3, 6)") => (every-checker
                                      double?
                                      (roughly 0.111111111))))

       (fact "the 'mod' function can be used to calculate remainders"
             (let [i (setup-interpreter)]
               (i "mod(13, 5)") => 3
               (i "mod(-13, 5)") => -3
               (i "mod(13, -5)") => 3
               (i "mod(-13, -5)") => -3
               (i "mod(13.5, 5)") => 3.5))

       (fact "the 'pow' function can be used to pow two numbers"
             (let [i (setup-interpreter)]
               (i "pow(3, 2)") => 9.0))

       (fact "the 'to-int' function casts a number to an integer"
             (let [i (setup-interpreter)]
               (i "to-int(123)") => 123
               (i "to-int(12.34)") => 12
               (i "to-int(12.99)") => 12
               (i "to-int(-12.34)") => -12))

       (fact "the 'eq?' function can be used to check equality of two numbers"
             (let [i (setup-interpreter)]
               (i "eq?(3, 3)") => 0
               (i "eq?(3, 5)") => nil
               (i "eq?(3, 3, 3)") => 0
               (i "eq?(3, 3, 5)") => nil))
       
       (fact "the 'lt?' function can be used to check if one number is less than another"
             (let [i (setup-interpreter)]
               (i "lt?(2, 3)") => 0
               (i "lt?(3, 3)") => nil
               (i "lt?(4, 3)") => nil
               (i "lt?(2, 3, 4)") => 0
               (i "lt?(2, 3, 1)") => nil))

       (fact "a lambda function without arguments"
             (let [i (setup-interpreter)]
               (i "x <- 1")
               (i "y <- 2")
               (i "foo <- { || add(x, y) }")
               (i "foo()") => 3))
       
       (fact "a lambda function with arguments can be invoked (and has a local environment)"
             (let [i (setup-interpreter)]
               (i "x <- 1")
               (i "y <- 2")
               (i "foo <- { |x| add(x, y) }")
               (i "foo(3)") => 5))

       (fact "the value of a reference to a binding outside of a lambda's scope is its new value when overridden at a later point"
             (let [i (setup-interpreter)]
               (i "foo <- 1")
               (i "bar <- { || foo }")
               (i "bar()") => 1
               (i "foo <- 2")
               (i "bar()") => 2))

       (fact "a list expression can be evaluated"
             (let [i (setup-interpreter)]
               (i "{ a <- 1 a }") => 1
               (i "b <- { a <- 1 a }")
               (i "b") => 1))

       (fact "the special symbol '%' can be used to refer to the value of the last statement"
             (let [i (setup-interpreter)]
               (i "123")
               (i "%") => 123
               (i "add(%, 1)") => 124))

       (fact "a lambda can be defined using a variant of the definition syntax"
             (let [i (setup-interpreter)]
               (i "foo(x) <- add(x, 1)")
               (i "foo(2)") => 3))

       (fact "a lambda can refer to itself with the identifier 'self'"
             (let [i (setup-interpreter)]
               (i "foo() <- self")
               (i "foo()") => fn?))

       (fact "an anonymous lambda can refer to itself with the identifier 'self'"
             (let [i (setup-interpreter)]
               (i "{ || self }")
               (i "%()") => fn?))

       (fact "the identifier 'self' has no meaning outside of a lambda"
             (let [i (setup-interpreter)]
               (i "self") => nil?))

       (fact "in a lambda with an argument called 'self', the identifier 'self' must refer to that argument instead of to the lambda"
             (let [i (setup-interpreter)]
               (i "foo(self) <- self")
               (i "foo(123)") => 123))

       (fact "a lambda can refer to itself by its own name"
             (let [i (setup-interpreter)]
               (i "foo() <- foo")
               (i "foo()") => fn?))

       (fact "a conditional expression evaluates to its consequent if its condition is truthy"
             (let [i (setup-interpreter)]
               (i "[0 : 123]") => 123))

       (fact "a conditional expression evaluates to nil if its condition is falsey"
             (let [i (setup-interpreter)]
               (i "[nil : 123]") => nil?))

       (fact "a conditional expression can have more than one clause"
             (let [i (setup-interpreter)]
               (i "[nil : 123, 0 : 456]") => 456))
       
       (fact "a multi-clause conditional expression tests all left-hand predicate expressions from first to last and then evaluates the right-hand expression of whichever is truthy"
             (let [i (setup-interpreter)]
               (i "[nil : 123, 1 : 456, 2 : 789]") => 456))
       
       (fact "a multi-clause conditional expression evaluates to nil if none of the predicates are truthy"
             (let [i (setup-interpreter)]
               (i "[nil : 123, nil : 456]") => nil?))

       (fact "a lambda can recur by invoking itself"
             (let [i (setup-interpreter)]
               (i "dec(n) <- add(n, -1)")
               (i "multiply(a, b) <- [eq?(b, 0) : 0, :else : add(a, multiply(a, dec(b)))]")
               (i "multiply(2, 3)") => 6))

       (fact "two lambdas can invoke each other recursively"
             (let [i (setup-interpreter)]
               (i "foo(n) <- [eq?(n, 1) : bar(2), eq?(n, 3) : 123]")
               (i "bar(n) <- [eq?(n, 2) : foo(3)]")
               (i "foo(1)") => 123))

       (fact "lambdas can have multi-statement bodies"
             (let [i (setup-interpreter)]
               (i "inc(n) <- add(n, 1)")
               (i "double(n) <- add(n, n)")
               (i "incdouble(n) <- { n inc(%) double(%) }")
               (i "incdouble(5)") => 12))

       (fact "lambdas can be nested"
             (let [i (setup-interpreter)]
               (i "{ |a| { |b| a } }")
               (i "%(2)")
               (i "%(3)") => 2
               (i "{ |a| { |b| add(a, b) } }")
               (i "%(2)")
               (i "%(3)") => 5))

       (fact "lambdas can be higher-order"
             (let [i (setup-interpreter)]
               (i "inc(n) <- add(n, 1)")
               (i "double(n) <- add(n, n)")
               (i "compose(f, g) <- { |n| f(g(n)) }")
               (i "incdouble <- compose(double, inc)")
               (i "incdouble(5)") => 12))

       (fact "loading from a file path"
             (let [i (setup-interpreter)]
               (i "@load \"path/to/file\"") => "*loaded 'path/to/file'*"
               (provided
                (l/load-code-from-file-path! test-loader-settings anything "path/to/file") => 123 :times 1)))
       
       (fact "loading from a lib spec"
             (let [i (setup-interpreter)]
               (i "@load spec.for.lib") => "*loaded 'spec.for.lib'*"
               (provided
                (l/load-code-from-lib-spec! test-loader-settings anything ["spec" "for" "lib"]) => 123 :times 1)))

       (fact "requiring from a file path"
             (let [i (setup-interpreter)]
               (i "@require \"path/to/file\"") => "*loaded 'path/to/file'*"
               (provided
                (l/load-code-from-file-path! test-loader-settings anything "path/to/file") => 123 :times 1)
               (i "@require \"path/to/file\"") => "*already loaded 'path/to/file'*"
               (provided
                (l/load-code-from-file-path! anything anything anything) => 123 :times 0)))
       
       (fact "requiring from a lib spec"
             (let [i (setup-interpreter)]
               (i "@require spec.for.lib") => "*loaded 'spec.for.lib'*"
               (provided
                (l/load-code-from-lib-spec! test-loader-settings anything ["spec" "for" "lib"]) => 123 :times 1)
               (i "@require spec.for.lib") => "*already loaded 'spec.for.lib'*"
               (provided
                (l/load-code-from-lib-spec! anything anything anything) => 123 :times 0)))

       (fact "cons produces a pair of values"
             (let [i (setup-interpreter)]
               (i "cons(1, 2)") => (list 1 2)))
       
       (fact "car retrieves the left-hand value from a pair"
             (let [i (setup-interpreter)]
               (i "car(cons(1, 2))") => 1))

       (fact "cdr retrieves the right-hand value from a pair"
             (let [i (setup-interpreter)]
               (i "cdr(cons(1, 2))") => 2))

       (fact "lambdas can have varargs in their binding pattern"
             (let [i (setup-interpreter)]
               (i "{ |x, y...| add(x, car(y)) }")
               (i "%(1, 2, 3)") => 3))
       
       (fact "lambda definitions can have varargs in their binding pattern"
             (let [i (setup-interpreter)]
               (i "f(x, y...) <- add(x, car(y))")
               (i "f(1, 2, 3)") => 3))

       (fact "invocations can have varexprs, which unrolls a logos list to become an argument list"
             (let [i (setup-interpreter)]
               (i "f(x, y, z) <- z")
               (i "f(cons(1, cons(2, cons(3, nil)))...)") => 3))

       (fact "a varexpr can be bound to a vararg"
             (let [i (setup-interpreter)]
               (i "f(x, y, z...) <- car(z)")
               (i "f(1, 2, cons(3, cons(4, cons(5, nil)))...)") => 3))

       (fact "a varexpr can be bound to a combintion of args and a vararg"
             (let [i (setup-interpreter)]
               (i "f(x, y, z...) <- add(y, car(z))")
               (i "f(1, cons(2, cons(3, cons(4, nil)))...)") => 5))

       (fact "rand generates a random decimal number between 0 and 1"
             (let [i (setup-interpreter)]
               (i "rand()") => 0.123
               (provided
                (i/rand*) => 0.123))))
