(ns logos-clj.literal-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.logos-type :as lt]
            [logos-clj.methods]))

(facts "about literal analysis"
       (fact "analyzes int literals"
             (ast/logos-analyze [:int-literal "123"])
             => (eval/->stmt :literal 123))
       
       (fact "analyzes decimal literals"
             (ast/logos-analyze [:decimal-literal "12.34"])
             => (eval/->stmt :literal 12.34))
       
       (fact "analyzes keyword literals"
             (ast/logos-analyze [:keyword-literal "foo"])
             => (eval/->stmt :literal :foo))
       
       (fact "analyzes string literals"
             (ast/logos-analyze [:string-literal "foo"])
             => (eval/->stmt :literal (lt/clj-string->logos-string "foo"))))

(facts "about literal evaluation"
       (fact "evaluates int literals"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :literal 123))]
               env* => env
               val => 123))

       (fact "evaluates decimal literals"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :literal 12.34))]
               env* => env
               val => 12.34))
       
       (fact "evaluates keyword literals"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :literal :foo))]
               env* => env
               val => :foo))
       
       (fact "evaluates string literals"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :literal (lt/clj-string->logos-string "foo")))]
               env* => env
               val => (lt/clj-string->logos-string "foo"))))
