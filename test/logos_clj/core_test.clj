(ns logos-clj.core-test
  (:require [midje.sweet :refer :all]
            [logos-clj.core :as c]))

(facts "about parsing command line arguments"
       (fact "if the -i option is specified, the program is started in interactive mode"
             (let [parsed-args (c/parse-args (list "-i"))]
               (c/args-valid? parsed-args) => truthy
               (c/interactive-mode? parsed-args) => truthy))
       
       (fact "if the -i option is not specified, the program is started in non-interactive mode"
             (let [parsed-args (c/parse-args (list))]
               (c/args-valid? parsed-args) => truthy
               (c/interactive-mode? parsed-args) => falsey))

       (fact "if the -f option is specified, the given path is the file path"
             (let [parsed-args (c/parse-args (list "-f" "/path/to/file"))]
               (c/args-valid? parsed-args) => truthy
               (c/get-file-path parsed-args) => "/path/to/file"))

       (fact "if the -f option is not specified, there is no file path"
             (let [parsed-args (c/parse-args (list))]
               (c/args-valid? parsed-args) => truthy
               (c/get-file-path parsed-args) => nil?))

       (fact "if the -f option is specified, but no path is given, the arguments are invalid"
             (let [parsed-args (c/parse-args (list "-f"))]
               (c/args-valid? parsed-args) => falsey))

       (fact "if the -i and -f option are both specified, there is a file path and the program also starts in interactive mode"
             (let [parsed-args (c/parse-args (list "-i" "-f" "/path/to/file"))]
               (c/args-valid? parsed-args) => truthy
               (c/interactive-mode? parsed-args) => truthy
               (c/get-file-path parsed-args) => "/path/to/file"))
       
       (fact "if the -i and -f option are both specified, there is a file path and the program also starts in interactive mode"
             (let [parsed-args (c/parse-args (list "-f" "/path/to/file" "-i"))]
               (c/args-valid? parsed-args) => truthy
               (c/interactive-mode? parsed-args) => truthy
               (c/get-file-path parsed-args) => "/path/to/file")))
