(ns logos-clj.parser-test
  (:require [midje.sweet :refer :all]
            [logos-clj.parser :as prs]
            [logos-clj.ast :as ast]))

(facts "about logos-parse"
       (fact "parses a string of Logos code into a list of statement ASTs"
             (let [parse-result (prs/logos-parse "a <- 1 b <- 2 c <- 3")]
               (prs/failure? parse-result) => falsey
               parse-result
               => [:command-list
                   [:statement [:basic-definition [:identifier "a"] [:int-literal "1"]]]
                   [:statement [:basic-definition [:identifier "b"] [:int-literal "2"]]]
                   [:statement [:basic-definition [:identifier "c"] [:int-literal "3"]]]]))

       (fact "returns a failure objects containing line and column on syntax errors"
             (let [parse-result (prs/logos-parse "abc <- 123\ndef <- <- 456\nghi <- 789")]
               (prs/failure? parse-result) => truthy
               (prs/failure-text parse-result) => "def <- <- 456"
               (prs/failure-line parse-result) => 2
               (prs/failure-column parse-result) => 8))

       (fact "if a string could be syntactically valid if more was added to it, it's a failure due to incompleteness"
             (let [string "a <- "
                   parse-result (prs/logos-parse string)]
               (prs/failure? parse-result) => truthy
               (prs/incomplete? parse-result string) => truthy))

       (fact "if a string could not be syntactically valid if more was added to it, it's not a failure due to incompleteness"
             (let [string "a <- <-"
                   parse-result (prs/logos-parse string)]
               (prs/failure? parse-result) => truthy
               (prs/incomplete? parse-result string) => falsey))

       (fact "can parse an empty list of statements"
             (prs/logos-parse "  ")
             => [:command-list])

       (fact "can handle whitespace in front of a statement"
             (prs/logos-parse "  123")
             => [:command-list
                 [:statement [:int-literal "123"]]])

       (fact "can handle multiple statements with whitespace inbetween them"
             (prs/logos-parse "  123  456")
             => [:command-list
                 [:statement [:int-literal "123"]]
                 [:statement [:int-literal "456"]]])

       (fact "whitespace can occur at the end"
             (prs/logos-parse "123\n")
             => [:command-list
                 [:statement [:int-literal "123"]]])

       (fact "whitespace can include spaces, carriage returns, line feeds or tabs"
             (prs/logos-parse "1 2\r3\n4\t5\r\n\t6")
             => [:command-list
                 [:statement [:int-literal "1"]]
                 [:statement [:int-literal "2"]]
                 [:statement [:int-literal "3"]]
                 [:statement [:int-literal "4"]]
                 [:statement [:int-literal "5"]]
                 [:statement [:int-literal "6"]]])

       (fact "code can contain comments"
             (prs/logos-parse "123 # this is a comment\n456")
             => [:command-list
                 [:statement [:int-literal "123"]]
                 [:statement [:int-literal "456"]]])
       
       (fact "comments can occur at the end"
             (prs/logos-parse "123 # this is a comment\n456 # this is another comment")
             => [:command-list
                 [:statement [:int-literal "123"]]
                 [:statement [:int-literal "456"]]])

       (fact "more than one comment can occur at the beginning"
             (prs/logos-parse "# this is a comment\n\n123")
             => [:command-list
                 [:statement [:int-literal "123"]]])
       
       (fact "parses int literals"
             (prs/logos-parse "123")
             => [:command-list
                 [:statement [:int-literal "123"]]])

       (fact "parses keyword literals"
             (prs/logos-parse ":foo")
             => [:command-list
                 [:statement [:keyword-literal "foo"]]])

       (fact "keyword literals can contain caps, digits and a certain set of special symbols"
             (prs/logos-parse ":Foo123-_+*$?!")
             => [:command-list
                 [:statement [:keyword-literal "Foo123-_+*$?!"]]])

       (fact "parses string literals"
             (prs/logos-parse "\"foo\"")
             => [:command-list
                 [:statement [:string-literal "foo"]]])

       (fact "string literals can contain any character except double quotes"
             (prs/logos-parse "\"foo$3@#'()[]{}-_&éà\"")
             => [:command-list
                 [:statement [:string-literal "foo$3@#'()[]{}-_&éà"]]])
       
       (fact "string literals can be empty"
             (prs/logos-parse "\"\"")
             => [:command-list
                 [:statement [:string-literal ""]]])

       (fact "parses decimal literals"
             (prs/logos-parse "12.34")
             => [:command-list
                 [:statement [:decimal-literal "12.34"]]])

       (fact "parses identifiers"
             (prs/logos-parse "foo")
             => [:command-list
                 [:statement [:identifier "foo"]]])

       (fact "identifiers can contain caps, digits and a certain set of special symbols"
             (prs/logos-parse "Foo123-_+*$?!")
             => [:command-list
                 [:statement [:identifier "Foo123-_+*$?!"]]])

       (fact "identifiers cannot start with a digit"
             (prs/logos-parse "1foo")
             => prs/failure?)
       
       (fact "identifiers cannot start with a dash"
             (prs/logos-parse "-foo")
             => prs/failure?)

       (fact "parses last-vals"
             (prs/logos-parse "%")
             => [:command-list
                 [:statement [:last-val]]])

       (fact "parses definitions"
             (prs/logos-parse "foo <- 123")
             => [:command-list
                 [:statement
                  [:basic-definition
                   [:identifier "foo"]
                   [:int-literal "123"]]]])

       (fact "parses invocations without arguments"
             (prs/logos-parse "foo()")
             => [:command-list
                 [:statement
                  [:invocation
                   [:identifier "foo"]]]])

       (fact "parses invocations with one argument"
             (prs/logos-parse "foo(2)")
             => [:command-list
                 [:statement
                  [:invocation
                   [:identifier "foo"]
                   [:int-literal "2"]]]])

       (fact "parses invocations with two arguments"
             (prs/logos-parse "foo(2, 3)")
             => [:command-list
                 [:statement
                  [:invocation
                   [:identifier "foo"]
                   [:int-literal "2"]
                   [:int-literal "3"]]]])

       (fact "parses invocations with more than two arguments"
             (prs/logos-parse "foo(2, 3, 4)")
             => [:command-list
                 [:statement
                  [:invocation
                   [:identifier "foo"]
                   [:int-literal "2"]
                   [:int-literal "3"]
                   [:int-literal "4"]]]])
       
       (fact "invocations can have a varexpr at the end"
             (prs/logos-parse "foo(x, y, z...)")
             => [:command-list
                 [:statement
                  [:invocation
                   [:identifier "foo"]
                   [:identifier "x"]
                   [:identifier "y"]
                   [:varexpr
                    [:identifier "z"]]]]])
       
       (fact "invocations can have just a varexpr and nothing else"
             (prs/logos-parse "foo(x...)")
             => [:command-list
                 [:statement
                  [:invocation
                   [:identifier "foo"]
                   [:varexpr
                    [:identifier "x"]]]]])
       
       (fact "invocations cannot have a varexpr that's not at the end"
             (prs/logos-parse "foo(x, y..., z)")
             => prs/failure?)
       
       (fact "invocations can't have more than one varexpr"
             (prs/logos-parse "foo(x, y..., z...)")
             => prs/failure?)

       (fact "parses lambda expressions without arguments"
             (prs/logos-parse "{ || 123 }")
             => [:command-list
                 [:statement
                  [:lambda
                   [:binding-pattern]
                   [:statement-list
                    [:statement
                     [:int-literal "123"]]]]]])

       (fact "parses lambda expressions with one argument"
             (prs/logos-parse "{ |x| 123 }")
             => [:command-list
                 [:statement
                  [:lambda
                   [:binding-pattern
                    [:arg "x"]]
                   [:statement-list
                    [:statement
                     [:int-literal "123"]]]]]])
       
       (fact "parses lambda expressions with two arguments"
             (prs/logos-parse "{ |x, y| 123 }")
             => [:command-list
                 [:statement
                  [:lambda
                   [:binding-pattern
                    [:arg "x"]
                    [:arg "y"]]
                   [:statement-list
                    [:statement
                     [:int-literal "123"]]]]]])

       (fact "parses lambda expressions with two arguments"
             (prs/logos-parse "{ |x, y, z| 123 }")
             => [:command-list
                 [:statement
                  [:lambda
                   [:binding-pattern
                    [:arg "x"]
                    [:arg "y"]
                    [:arg "z"]]
                   [:statement-list
                    [:statement
                     [:int-literal "123"]]]]]])

       (fact "parses lambda expressions with more than one statement in its body"
             (prs/logos-parse "{ |x| 123 456 }")
             => [:command-list
                 [:statement
                  [:lambda
                   [:binding-pattern
                    [:arg "x"]]
                   [:statement-list
                    [:statement
                     [:int-literal "123"]]
                    [:statement
                     [:int-literal "456"]]]]]])

       (fact "lambda expressions can have a vararg at the end"
             (prs/logos-parse "{ |x, y, z...| 123 }")
             => [:command-list
                 [:statement
                  [:lambda
                   [:binding-pattern
                    [:arg "x"]
                    [:arg "y"]
                    [:vararg "z"]]
                   [:statement-list
                    [:statement
                     [:int-literal "123"]]]]]])
       
       (fact "lambda expressions can have just a vararg and nothing else"
             (prs/logos-parse "{ |x...| 123 }")
             => [:command-list
                 [:statement
                  [:lambda
                   [:binding-pattern
                    [:vararg "x"]]
                   [:statement-list
                    [:statement
                     [:int-literal "123"]]]]]])
       
       (fact "lambda expressions cannot have a vararg that's not at the end"
             (prs/logos-parse "{ |x, y..., z| 123 }")
             => prs/failure?)
       
       (fact "lambda expressions can't have more than one vararg"
             (prs/logos-parse "{ |x, y..., z...| 123 }")
             => prs/failure?)
       
       (fact "parses list expressions"
             (prs/logos-parse "{ a <- 1 a }")
             => [:command-list
                 [:statement
                  [:statement-list
                   [:statement
                    [:basic-definition
                     [:identifier "a"]
                     [:int-literal "1"]]]
                   [:statement
                    [:identifier "a"]]]]])

       (fact "list expressions can start with multiple whitespaces"
             (prs/logos-parse "{ a <- 1 a }")
             => [:command-list
                 [:statement
                  [:statement-list
                   [:statement
                    [:basic-definition
                     [:identifier "a"]
                     [:int-literal "1"]]]
                   [:statement
                    [:identifier "a"]]]]])

       (fact "parses lambda definitions"
             (prs/logos-parse "foo(x, y) <- x")
             => [:command-list
                 [:statement
                  [:lambda-definition
                   [:identifier "foo"]
                   [:binding-pattern
                    [:arg "x"]
                    [:arg "y"]]
                   [:identifier "x"]]]])

       (fact "parses lambda definitions with varargs"
             (prs/logos-parse "foo(x, y...) <- x")
             => [:command-list
                 [:statement
                  [:lambda-definition
                   [:identifier "foo"]
                   [:binding-pattern
                    [:arg "x"]
                    [:vararg "y"]]
                   [:identifier "x"]]]])

       (fact "parses conditional expressions without an alternative clause"
             (prs/logos-parse "[1 : 2]")
             => [:command-list
                 [:statement
                  [:conditional
                   [:conditional-clause
                    [:int-literal "1"]
                    [:int-literal "2"]]]]])

       (fact "parses conditional expressions with an alternative clause"
             (prs/logos-parse "[1 : 2, 0 : 3]")
             => [:command-list
                 [:statement
                  [:conditional
                   [:conditional-clause
                    [:int-literal "1"]
                    [:int-literal "2"]]
                   [:conditional-clause
                    [:int-literal "0"]
                    [:int-literal "3"]]]]])

       (fact "parses conditional expressions with a chain of alternatives"
             (prs/logos-parse "[1 : 2, 3 : 4, 0 : 5]")
             => [:command-list
                 [:statement
                  [:conditional
                   [:conditional-clause
                    [:int-literal "1"]
                    [:int-literal "2"]]
                   [:conditional-clause
                    [:int-literal "3"]
                    [:int-literal "4"]]
                   [:conditional-clause
                    [:int-literal "0"]
                    [:int-literal "5"]]]]])

       (fact "commas at the end of conditional clauses are optional"
             (prs/logos-parse "[1 : 2  3 : 4  0 : 5]")
             => [:command-list
                 [:statement
                  [:conditional
                   [:conditional-clause
                    [:int-literal "1"]
                    [:int-literal "2"]]
                   [:conditional-clause
                    [:int-literal "3"]
                    [:int-literal "4"]]
                   [:conditional-clause
                    [:int-literal "0"]
                    [:int-literal "5"]]]]])

       (fact "parses directives to load from file path"
             (prs/logos-parse "@load \"path/to/file\"")
             => [:command-list
                 [:load-file
                  [:string-literal "path/to/file"]]])
       
       (fact "parses directives to load from lib spec"
             (prs/logos-parse "@load spec.for.lib")
             => [:command-list
                 [:load-lib
                  [:lib-spec "spec" "for" "lib"]]])

       (fact "a load lib spec can contain caps, digits, dashes and underscores"
             (prs/logos-parse "@load Sp-3c.f_O-r.l1_B")
             => [:command-list
                 [:load-lib
                  [:lib-spec "Sp-3c" "f_O-r" "l1_B"]]])
       
       (fact "a load lib spec can consist of only one part"
             (prs/logos-parse "@load spec")
             => [:command-list
                 [:load-lib
                  [:lib-spec "spec"]]])
       
       (fact "parses directives to require from file path"
             (prs/logos-parse "@require \"path/to/file\"")
             => [:command-list
                 [:require-file
                  [:string-literal "path/to/file"]]])
       
       (fact "parses directives to require from lib spec"
             (prs/logos-parse "@require spec.for.lib")
             => [:command-list
                 [:require-lib
                  [:lib-spec "spec" "for" "lib"]]])

       (fact "a require lib spec can contain caps, digits, dashes and underscores"
             (prs/logos-parse "@require Sp-3c.f_O-r.l1_B")
             => [:command-list
                 [:require-lib
                  [:lib-spec "Sp-3c" "f_O-r" "l1_B"]]])
       
       (fact "a require lib spec can consist of only one part"
             (prs/logos-parse "@require spec")
             => [:command-list
                 [:require-lib
                  [:lib-spec "spec"]]]))
