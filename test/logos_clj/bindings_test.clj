(ns logos-clj.bindings-test
  (:require [midje.sweet :refer :all]
            [logos-clj.bindings :as bind]
            [logos-clj.env :as env]))

(facts "about analyze-binding-pattern"
       (fact "analyzes binding pattern ASTs"
             (bind/analyze-binding-pattern [:binding-pattern
                                            [:arg "x"]
                                            [:arg "y"]
                                            [:vararg "z"]])
             => (list [:arg "x"]
                      [:arg "y"]
                      [:vararg "z"])))

(facts "about bind-values"
       (fact "produces a frame by matching a binding pattern against a list of values"
             (bind/bind-values (list [:arg "x"]
                                     [:arg "y"]
                                     [:arg "z"])
                               (list 1 2 3))
             => (list (env/->binding "x" 1)
                      (env/->binding "y" 2)
                      (env/->binding "z" 3)))
       
       (fact "an empty list of values can be bound by an empty pattern, which results in an empty frame"
             (bind/bind-values (list)
                               (list))
             => (list))

       (fact "when a vararg occurs at the tail end of a binding pattern, all remaining values are bound to its name as a Logos list"
             (bind/bind-values (list [:arg "x"]
                                     [:arg "y"]
                                     [:vararg "z"])
                               (list 1 2 3 4 5))
             => (list (env/->binding "x" 1)
                      (env/->binding "y" 2)
                      (env/->binding "z" (list 3 (list 4 (list 5 nil))))))

       (fact "a vararg can have just one value"
             (bind/bind-values (list [:arg "x"]
                                     [:arg "y"]
                                     [:vararg "z"])
                               (list 1 2 3))
             => (list (env/->binding "x" 1)
                      (env/->binding "y" 2)
                      (env/->binding "z" (list 3 nil))))
       
       (fact "a vararg can be empty"
             (bind/bind-values (list [:arg "x"]
                                     [:arg "y"]
                                     [:vararg "z"])
                               (list 1 2))
             => (list (env/->binding "x" 1)
                      (env/->binding "y" 2)
                      (env/->binding "z" nil))))
