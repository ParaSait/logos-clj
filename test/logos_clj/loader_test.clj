(ns logos-clj.loader-test
  (:require [midje.sweet :refer :all]
            [logos-clj.loader :as l]))

(facts "about ->loader-settings"
       (fact "constructs loader settings"
             (l/->loader-settings "lib")
             => {:lib-path "lib"}))

(facts "about resolve-lib-spec"
       (fact "a lib spec maps to the directory structure within the library path from the loader settings, with the last element of the spec being the basename of a file with '.lgs' extension"
             (l/resolve-lib-spec (l/->loader-settings "/lib/path")
                                 ["spec" "for" "lib"])
             => "/lib/path/spec/for/lib.lgs")

       (fact "throws an error if the library path from the loader settings is nil or empty"
             (l/resolve-lib-spec (l/->loader-settings nil)
                                 ["spec" "for" "lib"])
             => (throws clojure.lang.ExceptionInfo)
             (l/resolve-lib-spec (l/->loader-settings "")
                                 ["spec" "for" "lib"])
             => (throws clojure.lang.ExceptionInfo)))
