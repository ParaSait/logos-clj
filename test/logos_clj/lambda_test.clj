(ns logos-clj.lambda-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.methods]))

(facts "about lambda analysis"
       (fact "analyzes lambda expressions"
             (ast/logos-analyze [:statement-list
                                 [:lambda
                                  [:binding-pattern
                                   [:arg "x"]
                                   [:arg "y"]
                                   [:arg "z"]]
                                  [:statement-list
                                   [:int-literal "123"]]]])
             => (eval/->stmt :lambda
                             (list [:arg "x"]
                                   [:arg "y"]
                                   [:arg "z"])
                             [:literal 123])))

(facts "about lambda evaluation"
       (fact "evaluates lambda expressions"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :lambda
                                                                (list [:arg "x"]
                                                                      [:arg "y"]
                                                                      [:arg "z"])
                                                                [:literal 123]))]
               env* => env
               val => fn?)))
