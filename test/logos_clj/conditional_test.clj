(ns logos-clj.conditional-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.methods]))

(facts "about conditional analysis"
       (fact "analyzes conditionals without an alternative clause"
             (ast/logos-analyze [:conditional
                                 [:conditional-clauses
                                  [:int-literal "1"]
                                  [:int-literal "2"]]])
             => (eval/->stmt :conditional
                             [:literal 1]
                             [:literal 2]))
       
       (fact "analyzes conditionals with an alternative clause"
             (ast/logos-analyze [:conditional
                                 [:conditional-clause
                                  [:int-literal "1"]
                                  [:int-literal "2"]]
                                 [:conditional-clause
                                  [:int-literal "0"]
                                  [:int-literal "3"]]])
             => (eval/->stmt :conditional
                             [:literal 1]
                             [:literal 2]
                             (eval/->stmt :conditional
                                          [:literal 0]
                                          [:literal 3]))))

(facts "about conditional evaluation"
       (fact "evaluates conditionals without an alternative clause of which the condition holds"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :conditional
                                                                [:literal 1]
                                                                [:literal 2]))]
               env* => env
               val => 2))
       
       (fact "evaluates conditionals without an alternative clause of which the condition does not hold"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "nil" nil)]))
                   [env* val] (eval/logos-eval env (eval/->stmt :conditional
                                                                [:identifier "nil"]
                                                                [:literal 2]))]
               env* => env
               val => nil?))
       
       (fact "evaluates conditionals with an alternative clause of which the condition holds"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :conditional
                                                                [:literal 1]
                                                                [:literal 2]
                                                                [:literal 3]))]
               env* => env
               val => 2))
       
       (fact "evaluates conditionals with an alternative clause of which the condition does not hold"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "nil" nil)]))
                   [env* val] (eval/logos-eval env (eval/->stmt :conditional
                                                                [:identifier "nil"]
                                                                [:literal 2]
                                                                [:literal 3]))]
               env* => env
               val => 3)))
