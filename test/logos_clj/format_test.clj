(ns logos-clj.format-test
  (:require [midje.sweet :refer :all]
            [logos-clj.format :as fmt]))

(facts "about repl-event"
       (fact "surrounds the given string with * characters"
             (fmt/repl-event "foo") => "*foo*"))

(facts "about single-quote"
       (fact "surrounds the given string with ' characters"
             (fmt/single-quote "foo") => "'foo'"))
