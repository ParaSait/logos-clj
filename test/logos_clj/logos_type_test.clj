(ns logos-clj.logos-type-test
  (:require [midje.sweet :refer :all]
            [logos-clj.logos-type :as lt]))

(facts "about logos-string?"
       (fact "a value is a logos string if it's a list that starts with the ':string' keyword"
             (lt/logos-string? (list :string (list 102 (list 111 (list 111 nil))))) => truthy)
       
       (fact "a value is not a logos string if it's a list that doesn't start with the ':string' keyword"
             (lt/logos-string? (list :not-so-string (list 102 (list 111 (list 111 nil))))) => falsey)
       
       (fact "a value is not a logos string if it's not a clojure list"
             (lt/logos-string? 123) => falsey))

(facts "about clj-string->logos-string"
       (fact "converts a clojure string to a logos string"
             (lt/clj-string->logos-string "foo") => (list :string (list 102 (list 111 (list 111 nil))))))

(facts "about logos-string->clj-string"
       (fact "converts a logos string to a clojure string"
             (lt/logos-string->clj-string (list :string (list 102 (list 111 (list 111 nil))))) => "foo"))
