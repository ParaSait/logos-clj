(ns logos-clj.identifier-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.methods]))

(facts "about identifier analysis"
       (fact "analyzes identifiers"
             (ast/logos-analyze [:identifier "foo"])
             => (eval/->stmt :identifier "foo")))

(facts "about identifier evaluation"
       (fact "evaluates identifiers"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo" 123)]))
                   [env* val] (eval/logos-eval env (eval/->stmt :identifier "foo"))]
               env* => env
               val => 123)))
