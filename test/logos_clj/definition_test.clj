(ns logos-clj.definition-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.methods]))

(facts "about basic definition analysis"
       (fact "analyzes basic definitions"
             (ast/logos-analyze [:basic-definition
                                 [:identifier "foo"]
                                 [:int-literal "123"]])
             => (eval/->stmt :define
                             "foo"
                             (eval/->stmt :literal 123))))

(facts "about lambda definition analysis"
       (fact "analyzes lambda definitions"
             (ast/logos-analyze [:lambda-definition
                                 [:identifier "foo"]
                                 [:binding-pattern
                                  [:arg "x"]
                                  [:arg "y"]]
                                 [:identifier "x"]])
             => (eval/->stmt :define
                             "foo"
                             (eval/->stmt :lambda
                                          (list [:arg "x"]
                                                [:arg "y"])
                                          [:identifier "x"]))))

(facts "about definition evaluation"
       (fact "evaluates definitions"
             (let [env (-> (env/->empty-env)
                           (env/push-frame))
                   [env* val] (eval/logos-eval env (eval/->stmt :define "foo" (eval/->stmt :literal 123)))]
               env* => env ;; it's the same env, but its local frame mutated
               (env/env-lookup env "foo") => 123
               (env/env-lookup env* "foo") => 123
               val => "*defined 'foo'*"))

       (fact "evaluates redefinitions"
             (let [env (-> (env/->empty-env)
                           (env/push-frame))
                   [env* val] (eval/logos-eval env (eval/->stmt :define "foo" (eval/->stmt :literal 123)))
                   [env** val] (eval/logos-eval env (eval/->stmt :define "foo" (eval/->stmt :literal 456)))]
               env* => env ;; it's the same env, but its local frame mutated
               env** => env
               (env/env-lookup env "foo") => 456
               (env/env-lookup env* "foo") => 456
               (env/env-lookup env** "foo") => 456
               val => "*redefined 'foo'*")))
