(ns logos-clj.directive-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.loader :as l]
            [logos-clj.methods]))

(facts "about load-file analysis"
       (fact "analyzes load-file directives"
             (ast/logos-analyze [:load-file
                                 [:string-literal "path/to/file"]])
             => (eval/->stmt :load-file "path/to/file")))

(facts "about load-lib analysis"
       (fact "analyzes load-lib directives"
             (ast/logos-analyze [:command-list
                                 [:load-lib
                                  [:lib-spec "spec" "for" "lib"]]])
             => (eval/->stmt :load-lib ["spec" "for" "lib"])))

(facts "about require-file analysis"
       (fact "analyzes require-file directives"
             (ast/logos-analyze [:require-file
                                 [:string-literal "path/to/file"]])
             => (eval/->stmt :require-file "path/to/file")))

(facts "about require-lib analysis"
       (fact "analyzes require-lib directives"
             (ast/logos-analyze [:command-list
                                 [:require-lib
                                  [:lib-spec "spec" "for" "lib"]]])
             => (eval/->stmt :require-lib ["spec" "for" "lib"])))
