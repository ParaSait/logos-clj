(ns logos-clj.logos-lib-test
  (:require [midje.sweet :refer :all]
            [logos-clj.interpreter :as i]
            [logos-clj.logos-type :as lt]
            [logos-clj.loader :as l]))

(def ^{:private true} test-loader-settings
  (l/->loader-settings "lib"))

(defn- setup-interpreter
  []
  (binding [i/*handle-eval-errors* false] ;; FIXME this rebind is not working
    (let [i (i/->interpreter test-loader-settings)]
      (i "@require core")
      i)))

(def ^{:private true} i
  (atom nil))

(defn- init-interpreter!
  []
  (reset! i (setup-interpreter)))

(with-state-changes [(before :contents (init-interpreter!))]

  (facts "about core.lang/else"
         (fact "it's 0"
               (@i "else") => 0))

  (facts "about core.boolean/true"
         (fact "it's 0"
               (@i "true") => 0))

  (facts "about core.boolean/false"
         (fact "it's nil"
               (@i "false") => nil))

  (facts "about core.boolean/not"
         (fact "implements logical not"
               (@i "eq?(not(true), false)") => 0
               (@i "eq?(not(false), true)") => 0))

  (facts "about core.boolean/and"
         (fact "implements logical 'and', returning the last value when true"
               (@i "eq?(and(), true)") => 0
               (@i "eq?(and(1), 1)") => 0
               (@i "eq?(and(false), false)") => 0
               (@i "eq?(and(1, 2), 2)") => 0
               (@i "eq?(and(1, false), false)") => 0
               (@i "eq?(and(false, 2), false)") => 0
               (@i "eq?(and(false, false), false)") => 0
               (@i "eq?(and(1, 2, 3), 3)") => 0
               (@i "eq?(and(1, false, 3), false)") => 0))

  (facts "about core.boolean/or"
         (fact "implements logical or"
               (@i "eq?(or(), false)") => 0
               (@i "eq?(or(1), 1)") => 0
               (@i "eq?(or(false), false)") => 0
               (@i "eq?(or(1, 2), 1)") => 0
               (@i "eq?(or(1, false), 1)") => 0
               (@i "eq?(or(false, 2), 2)") => 0
               (@i "eq?(or(false, false), false)") => 0
               (@i "eq?(or(false, 2, false), 2)") => 0
               (@i "eq?(or(false, false, false), false)") => 0))

  (facts "about core.number/inc"
         (fact "increments a number"
               (@i "inc(3)") => 4))

  (facts "about core.number/dec"
         (fact "increments a number"
               (@i "dec(3)") => 2))

  (facts "about core.number/negate"
         (fact "negates a number"
               (@i "negate(3)") => -3))

  (facts "about core.number/invert"
         (fact "inverts a number"
               (@i "invert(3)") => (roughly 0.3333333)))

  (facts "about core.number/even?"
         (fact "checks if an integer is even"
               (@i "even?(0)") => truthy
               (@i "even?(3)") => falsey
               (@i "even?(4)") => truthy))

  (facts "about core.number/odd?"
         (fact "checks if an integer is odd"
               (@i "odd?(0)") => falsey
               (@i "odd?(3)") => truthy
               (@i "odd?(4)") => falsey))

  (facts "about core.number/gt?"
         (fact "checks if one number is greater than another"
               (@i "gt?(2, 3)") => nil
               (@i "gt?(3, 3)") => nil
               (@i "gt?(4, 3)") => 0
               (@i "gt?(4, 3, 2)") => 0
               (@i "gt?(4, 3, 5)") => nil))

  (facts "about core.number/lt-eq?"
         (fact "checks if one number is less than or equal to another"
               (@i "lt-eq?(2, 3)") => 0
               (@i "lt-eq?(3, 3)") => 0
               (@i "lt-eq?(4, 3)") => nil
               (@i "lt-eq?(2, 3, 3)") => 0
               (@i "lt-eq?(2, 3, 1)") => nil))

  (facts "about core.number/gt-eq?"
         (fact "checks if one number is greater than or equal to another"
               (@i "gt-eq?(2, 3)") => nil
               (@i "gt-eq?(3, 3)") => 0
               (@i "gt-eq?(4, 3)") => 0
               (@i "gt-eq?(4, 3, 2)") => 0
               (@i "gt-eq?(4, 3, 5)") => nil))

  (facts "about core.number/abs"
         (fact "returns absolute value of a number"
               (@i "abs(3)") => 3
               (@i "abs(0)") => 0
               (@i "abs(-3)") => 3))

  (facts "about core.number/range"
         (fact "returns an exclusive range with the given integers as its boundaries"
               (@i "range(3, 6)") => (list 3 (list 4 (list 5 nil))))
       
         (fact "can be a range from a higher integer to a lower one"
               (@i "range(6, 3)") => (list 6 (list 5 (list 4 nil))))

         (fact "if the given boundaries are equal, the range is an empty list"
               (@i "range(3, 3)") => nil))

  (facts "about core.number/rand-int"
         (fact "generates a random integer with given maximum"
               (@i "rand-int(10)") => 6
               (provided
                (i/rand*) => 0.678)))

  (facts "about core.list/list"
         (fact "produces a list with the given arguments as its elements"
               (@i "list(1, 2, 3)") => (list 1 (list 2 (list 3 nil)))
               (@i "list()") => nil))

  (facts "about core.list/nth"
         (fact "returns the item at the given index, or nil if there's no item at that index"
               (@i "l <- list(:a, :b, :c)")
               (@i "nth(l, 1)") => :b
               (@i "nth(l, 5)") => nil))

  (facts "about core.list/map"
         (fact "maps a list"
               (@i "list(1, 2, 3)")
               (@i "map(%, inc)") => (list 2 (list 3 (list 4 nil))))
       
         (fact "returns nil if the given list is nil"
               (@i "map(nil, inc)") => nil))

  (facts "about core.list/reduce"
         (fact "reduces a list"
               (@i "list(1, 2, 3)")
               (@i "reduce(%, add)") => 6)

         (fact "returns the only element if the list contains one element"
               (@i "list(1)")
               (@i "reduce(%, add)") => 1)
       
         (fact "returns nil if the given list is nil"
               (@i "reduce(nil, add)") => nil))

  (facts "about core.list/filter"
         (fact "filters a list by a predicate"
               (@i "range(1, 10) filter(%, odd?) eq?(%, list(1, 3, 5, 7, 9))") => truthy))

  (facts "about core.list/concat"
         (fact "concatenates given lists"
               (@i "concat()")
               => nil
               (@i "concat(list(1, 2))")
               => (list 1 (list 2 nil))
               (@i "concat(list(1, 2), list(3, 4))")
               => (list 1 (list 2 (list 3 (list 4 nil))))
               (@i "concat(list(1, 2), list(3, 4), list(5, 6))")
               => (list 1 (list 2 (list 3 (list 4 (list 5 (list 6 nil))))))))

  (facts "about core.list/append"
         (fact "appends a value at the end of a list"
               (@i "l <- list(1, 2)")
               (@i "append(l, 3)") => (list 1 (list 2 (list 3 nil))))

         (fact "appending a value to a nil list yield a list with one item"
               (@i "append(nil, 1)") => (list 1 nil)))

  (facts "about core.list/count"
         (fact "counts the number of items in the list"
               (@i "list(1, 2, 3)")
               (@i "count(%)") => 3)
       
         (fact "number of items if 0 if it's the empty list"
               (@i "count(nil)") => 0))

  (facts "about core.list/take"
         (fact "returns a sublist of length n, starting from the first element"
               (@i "list(1, 2, 3, 4, 5) take(%, 3) eq?(%, list(1, 2, 3))") => truthy
               (@i "list(1, 2, 3, 4, 5) take(%, 0) eq?(%, list())") => truthy
               (@i "list(1, 2, 3, 4, 5) take(%, -5) eq?(%, list())") => truthy
               (@i "list(1, 2, 3, 4, 5) take(%, 7) eq?(%, list(1, 2, 3, 4, 5))") => truthy))

  (facts "about core.list/repeat"
         (fact "repeats the given value in a list of x items"
               (@i "repeat(7, 3) eq?(%, list(7, 7, 7))") => truthy
               (@i "repeat(7, 0) eq?(%, list())") => truthy
               (@i "repeat(7, -3) eq?(%, list())") => truthy))

  (facts "about core.list/find"
         (fact "finds the first element in a list that satisfies the given predicate"
               (@i "find(list(6, 8, 4, 3, 2, 6), odd?) eq?(%, 3)") => truthy
               (@i "find(list(6, 8, 4, 8, 2, 6), odd?) eq?(%, nil)") => truthy
               (@i "find(list(), odd?) eq?(%, nil)") => truthy))

  (facts "about core.function/itself"
         (fact "returns the argument"
               (@i "itself(123)") => 123))
  
  (facts "about core.function/always"
         (fact "creates a function that always return the given value, no matter what set of arguments"
               (@i "always(42) eq?(%(1, :foo, 1.2), 42)")))
  
  (facts "about core.function/compose"
         (fact "composes two functions"
               (@i "double(n) <- add(n, n)")
               (@i "compose() %(5)") => 5
               (@i "compose(double) %(5)") => 10
               (@i "compose(double, inc) %(5)") => 12
               (@i "compose(dec, double, inc) %(5)") => 11))

  (facts "about core.function/complement"
         (fact "returns the complement of the given predicate"
               (@i "is-three?(n) <- eq?(n, 3)")
               (@i "is-not-three? <- complement(is-three?)")
               (@i "is-not-three?(3)") => nil
               (@i "is-not-three?(4)") => 0))

  (facts "about core.function/apply"
         (fact "applies the given list as arguments to the function"
               (@i "apply(add, list(1, 2, 3)) eq?(%, 6)")))
  
  (facts "about core.function/lpart"
         (fact "creates a partial function (from the left)"
               (@i "lpart(div, 3) eq?(%(6), 0.5)")))

  (facts "about core.function/rpart"
         (fact "creates a partial function (from the right)"
               (@i "rpart(div, 3) eq?(%(6), 2)")))
  
  (facts "about core.function/repeatedly"
         (fact "creates a function that repeatedly applies the given function n times"
               (@i "repeatedly(inc, 3) eq?(%(2), 5)")))
  
  (facts "about core.string/make-string"
         (fact "converts a list of characters to a string"
               (@i "make-string(cons(102, cons(111, cons(111, nil))))") => (lt/clj-string->logos-string "foo")))

  (facts "about core.string/chars"
         (fact "returns the list of characters in a string"
               (@i "chars(\"foo\")") => (list 102 (list 111 (list 111 nil)))))

  (facts "about core.string/join"
         (fact "joins given strings"
               (@i "join()")
               => (lt/clj-string->logos-string "")
               (@i "join(\"foo\")")
               => (lt/clj-string->logos-string "foo")
               (@i "join(\"foo\", \"bar\")")
               => (lt/clj-string->logos-string "foobar")
               (@i "join(\"foo\", \"bar\", \"baz\")")
               => (lt/clj-string->logos-string "foobarbaz")))

  (facts "about core.string/linefeed"
         (fact "it's a string that contains the linefeed character"
               (@i "linefeed") => (lt/clj-string->logos-string "\n")))

  (facts "about core.parsing/atoi"
         (fact "it converts a string to an integer"
               (@i "atoi(\"5\")") => 5
               (@i "atoi(\"123\")") => 123
               (@i "atoi(\"0\")") => 0
               (@i "atoi(\"\")") => nil
               (@i "atoi(\"abc\")") => nil
               (@i "atoi(\"abc123\")") => nil
               (@i "atoi(\"123abc\")") => 123
               (@i "atoi(\"12a34\")") => 12
               (@i "atoi(\"+123\")") => 123
               (@i "atoi(\"-123\")") => -123
               (@i "atoi(\"+\")") => nil
               (@i "atoi(\"-\")") => nil
               (@i "atoi(\"12+34\")") => 12
               (@i "atoi(\"+12-34\")") => 12
               (@i "atoi(\"-12+34\")") => -12))

  (facts "about core.formatting/format-bool"
         (fact "formats a value as a boolean"
               (@i "format-bool(5)") => (lt/clj-string->logos-string "true")
               (@i "format-bool(0)") => (lt/clj-string->logos-string "true")
               (@i "format-bool(:foo)") => (lt/clj-string->logos-string "true")
               (@i "format-bool(nil)") => (lt/clj-string->logos-string "false")))
  
  (facts "about core.formatting/format-int"
         (fact "formats an integer"
               (@i "format-int(5)") => (lt/clj-string->logos-string "5")
               (@i "format-int(0)") => (lt/clj-string->logos-string "0")
               (@i "format-int(23)") => (lt/clj-string->logos-string "23")
               (@i "format-int(100)") => (lt/clj-string->logos-string "100")
               (@i "format-int(-123)") => (lt/clj-string->logos-string "-123")))

  (facts "about core.formatting/format-string"
         (fact "interpolates formatted values into a string using a format specifier"
               (@i "format-string(\"The %s is %i, it's %b!\", \"answer\", 42, :foo)") => (lt/clj-string->logos-string "The answer is 42, it's true!")
               (@i "format-string(\"%i,%i,%i\", 123, 456, 789)") => (lt/clj-string->logos-string "123,456,789")))

  (facts "about core.stdio/println"
         (fact "prints a string with a linefeed added at the end"
               (@i "println(\"foo\")") => nil
               (provided
                (i/print+flush "foo\n") => nil)))

  (facts "about core.stdio/printf"
         (fact "prints a formatted string"
               (@i "printf(\"The answer is %i!\", 42)") => nil
               (provided
                (i/print+flush "The answer is 42!") => nil)
               (@i "printf(\"The answer is %i... or, maybe %i!\", 42, 123)") => nil
               (provided
                (i/print+flush "The answer is 42... or, maybe 123!") => nil)))

  (facts "about core.stdio/printfln"
         (fact "prints a formatted string with a linefeed added at the end"
               (@i "printfln(\"The answer is %i!\", 42)") => nil
               (provided
                (i/print+flush "The answer is 42!\n") => nil)
               (@i "printfln(\"The answer is %i... or, maybe %i!\", 42, 123)") => nil
               (provided
                (i/print+flush "The answer is 42... or, maybe 123!\n") => nil)))

  (facts "about core.table"
         (fact "tput and tget respective put and get into and out of a table"
               (@i "tbl <- table()")
               (@i "tbl <- tput(tbl, :foo, 123)")
               (@i "tbl <- tput(tbl, :bar, 456)")
               (@i "tget(tbl, :foo)") => 123
               (@i "tget(tbl, :bar)") => 456
               (@i "tget(tbl, :baz)") => nil
               (@i "tbl <- tput(tbl, :foo, 789)")
               (@i "tget(tbl, :foo)") => 789
               (@i "tget(tbl, :bar)") => 456)

         (fact "tdel deletes an item from a table"
               (@i "tbl <- table()")
               (@i "tbl <- tput(tbl, :foo, 123)")
               (@i "tbl <- tput(tbl, :bar, 456)")
               (@i "tbl <- tdel(tbl, :foo)")
               (@i "tget(tbl, :foo)") => nil
               (@i "tget(tbl, :bar)") => 456)

         (fact "thas? tells you if a key is present (even if its entry's value may be nil)"
               (@i "tbl <- table()")
               (@i "tbl <- tput(tbl, :foo, 123)")
               (@i "tbl <- tput(tbl, :bar, nil)")
               (@i "thas?(tbl, :foo)") => 0
               (@i "thas?(tbl, :bar)") => 0
               (@i "thas?(tbl, :baz)") => nil)
         
         (fact "table constructs a table"
               (@i "table()") => nil
               (@i "tbl <- table(:foo)")
               (@i "tget(tbl, :foo)") => nil
               (@i "tbl <- table(:foo, 123)")
               (@i "tget(tbl, :foo)") => 123
               (@i "tbl <- table(:foo, 123, :bar)")
               (@i "tget(tbl, :foo)") => 123
               (@i "tget(tbl, :bar)") => nil
               (@i "tbl <- table(:foo, 123, :bar, 456)")
               (@i "tget(tbl, :foo)") => 123
               (@i "tget(tbl, :bar)") => 456)))

