(ns logos-clj.statement-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.methods]))

(facts "about command list analysis"
       (fact "analyzes noops"
             (ast/logos-analyze [:command-list])
             => [:noop])

       (fact "analyzes single commands"
             (ast/logos-analyze [:command-list
                                 [:identifier "a"]])
             => [:identifier "a"])

       (fact "analyzes command pairs"
             (ast/logos-analyze [:command-list
                                 [:identifier "a"]
                                 [:identifier "b"]])
             => [:command-pair [:identifier "a"]
                 [:identifier "b"]])

       (fact "analyzes chained command pairs"
             (ast/logos-analyze [:command-list
                                 [:identifier "a"]
                                 [:identifier "b"]
                                 [:identifier "c"]])
             => [:command-pair
                 [:identifier "a"]
                 [:command-pair
                  [:identifier "b"]
                  [:identifier "c"]]]))

(facts "about command pair evaluation"
       (fact "evaluates the first command and then the second"
             (let [env (-> (env/->empty-env)
                           (env/push-frame))
                   [env* val] (eval/logos-eval env (eval/->stmt :command-pair
                                                                (eval/->stmt :define "foo" (eval/->stmt :literal 123))
                                                                (eval/->stmt :identifier "foo")))]
               env* => env ;; it's the same env, but its local frame mutated
               (env/env-lookup env "foo") => 123
               (env/env-lookup env* "foo") => 123
               val => 123)))

(facts "about statement list analysis"
       (fact "analyzes noops"
             (ast/logos-analyze [:statement-list])
             => [:noop])

       (fact "analyzes single statements"
             (ast/logos-analyze [:statement-list
                                 [:identifier "a"]])
             => [:identifier "a"])

       (fact "analyzes statement pairs"
             (ast/logos-analyze [:statement-list
                                 [:identifier "a"]
                                 [:identifier "b"]])
             => [:statement-pair [:identifier "a"]
                 [:identifier "b"]])

       (fact "analyzes chained statement pairs"
             (ast/logos-analyze [:statement-list
                                 [:identifier "a"]
                                 [:identifier "b"]
                                 [:identifier "c"]])
             => [:statement-pair
                 [:identifier "a"]
                 [:statement-pair
                  [:identifier "b"]
                  [:identifier "c"]]]))

(facts "about statement analysis"
       (fact "analyzes a statement"
             (ast/logos-analyze [:statement [:int-literal "123"]])
             => [:statement [:literal 123]]))

(facts "about noop evaluation"
       (fact "evaluates to nil"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :noop))]
               env* => env
               val => nil)))

(facts "about statement evaluation"
       (fact "evaluates to and sets the env's last value to the value of its expression"
             (let [env (env/->empty-env)
                   [env* val] (eval/logos-eval env (eval/->stmt :statement (eval/->stmt :literal 123)))]
               env* => (env/set-last-val env 123)
               val => 123)))

(facts "about statement pair evaluation"
       (fact "evaluates the first statement and then the second"
             (let [env (-> (env/->empty-env)
                           (env/push-frame))
                   [env* val] (eval/logos-eval env (eval/->stmt :statement-pair
                                                                (eval/->stmt :define "foo" (eval/->stmt :literal 123))
                                                                (eval/->stmt :identifier "foo")))]
               env* => env ;; it's the same env, but its local frame mutated
               (env/env-lookup env "foo") => 123
               (env/env-lookup env* "foo") => 123
               val => 123)))
