(ns logos-clj.repl-test
  (:require [midje.sweet :refer :all]
            [logos-clj.interpreter :as i]
            [logos-clj.logos-type :as lt]
            [logos-clj.repl :as repl]))

(facts "about format-value"
       (fact "formatting a nil value"
             (repl/format-value nil) => "nil")
       
       (fact "formatting a fn value"
             (repl/format-value (fn [] nil)) => "[lambda]")
       
       (fact "formatting a pair value"
             (repl/format-value (list 1 (list 2 3))) => "(1 (2 3))")
       
       (fact "formatting a list value"
             (repl/format-value (list 1 nil)) => "(1)"
             (repl/format-value (list 1 (list 2 nil))) => "(1, 2)"
             (repl/format-value (list 1 (list 2 (list 3 nil)))) => "(1, 2, 3)")
       
       (fact "formatting a string value"
             (repl/format-value (lt/clj-string->logos-string "foo")) => "\"foo\"")

       (fact "a double value is rounded down to 10 decimal places"
             (repl/format-value 57.300000000007) => "57.3"
             (repl/format-value 57.30000000007) => "57.3000000001"
             (repl/format-value 57.30000000003) => "57.3"
             (repl/format-value 57.3000000003) => "57.3000000003")
       
       (fact "any other value is formatted the way clojure formats it"
             (repl/format-value 123) => 123))

(facts "about logos-repl"
       (fact "terminates when the 'exit' command is invoked"
             (repl/logos-repl (i/->interpreter nil)) => anything
             (provided
              (read-line) => "exit"
              (repl/print* anything) => nil :times (range)
              (repl/println* anything) => nil :times (range))))
