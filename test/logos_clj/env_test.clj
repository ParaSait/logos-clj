(ns logos-clj.env-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]))

(facts "about push-frame and env-lookup"
       (fact "a lookup can be performed on an env where a frame has been pushed with a binding for that name"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo" 123)]))]
               (env/env-lookup env "foo") => 123))

       (fact "there can be multiple bindings in a single frame"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo" 123)
                                            (env/->binding "bar" 456)]))]
               (env/env-lookup env "foo") => 123
               (env/env-lookup env "bar") => 456))

       (fact "multiple frames can be pushed on an env"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo" 123)
                                            (env/->binding "bar" 456)])
                           (env/push-frame [(env/->binding "baz" 789)
                                            (env/->binding "qux" 135)]))]
               (env/env-lookup env "foo") => 123
               (env/env-lookup env "bar") => 456
               (env/env-lookup env "baz") => 789
               (env/env-lookup env "qux") => 135))

       (fact "a binding can be overridden by another binding with the same name in subsequent frame"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo" 123)
                                            (env/->binding "bar" 456)])
                           (env/push-frame [(env/->binding "bar" 789)
                                            (env/->binding "baz" 135)]))]
               (env/env-lookup env "foo") => 123
               (env/env-lookup env "bar") => 789
               (env/env-lookup env "baz") => 135))

       (fact "a lookup of a name that's not found in the bindings yields nil"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo" 123)]))]
               (env/env-lookup env "baz") => nil?)))

(facts "about put-local-binding!"
       (fact "returns nil if the given binding's name is not bound in the local frame"
             (let [env (-> (env/->empty-env)
                           (env/push-frame))]
               (env/env-lookup env "foo") => nil?
               (env/put-local-binding! env (env/->binding "foo" 123)) => nil?
               (env/env-lookup env "foo") => 123))
       
       (fact "returns the previous binding if the given binding's name is not bound in the local frame"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo" 123)]))]
               (let [prev-binding (env/put-local-binding! env (env/->binding "foo" 456))]
                 (env/env-lookup env "foo") => 456
                 (env/put-local-binding! env prev-binding)
                 (env/env-lookup env "foo") => 123))))

(facts "about set-last-val and get-last-val"
       (fact "getting the last value will return nil if no last value has been set yet"
             (let [env (env/->empty-env)]
               (env/get-last-val env) => nil?))
       
       (fact "getting the last value will return the last value if one has been set"
             (let [env (-> (env/->empty-env)
                           (env/set-last-val 123))]
               (env/get-last-val env) => 123))

       (fact "getting the last value will return the latest last value that has been set"
             (let [env (-> (env/->empty-env)
                           (env/set-last-val 123)
                           (env/set-last-val 456))]
               (env/get-last-val env) => 456)))
