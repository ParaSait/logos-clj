(ns logos-clj.last-val-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.methods]))

(facts "about last-val analysis"
       (fact "analyzes last-vals"
             (ast/logos-analyze [:last-val])
             => (eval/->stmt :last-val)))

(facts "about last-val evaluation"
       (fact "evaluates last-vals"
             (let [env (-> (env/->empty-env)
                           (env/set-last-val 123))
                   [env* val] (eval/logos-eval env (eval/->stmt :last-val))]
               env* => env
               val => 123)))
