(ns logos-clj.invocation-test
  (:require [midje.sweet :refer :all]
            [logos-clj.env :as env]
            [logos-clj.ast :as ast]
            [logos-clj.eval :as eval]
            [logos-clj.methods]))

(facts "about invocation analysis"
       (fact "analyzes invocations without arguments"
             (ast/logos-analyze [:invocation
                                 [:identifier "foo"]])
             => (eval/->stmt :invoke
                             (eval/->stmt :identifier "foo")
                             (list)))

       (fact "analyzes invocations with one argument"
             (ast/logos-analyze [:invocation
                                 [:identifier "foo"]
                                 [:int-literal "2"]])
             => (eval/->stmt :invoke
                             (eval/->stmt :identifier "foo")
                             (list (eval/->stmt :literal 2))))

       (fact "analyzes invocations with more than one argument"
             (ast/logos-analyze [:invocation
                                 [:identifier "foo"]
                                 [:int-literal "2"]
                                 [:int-literal "3"]])
             => (eval/->stmt :invoke
                             (eval/->stmt :identifier "foo")
                             (list (eval/->stmt :literal 2)
                                   (eval/->stmt :literal 3))))
       
       (fact "analyzes invocations with a varexpr"
             (ast/logos-analyze [:invocation
                                 [:identifier "foo"]
                                 [:int-literal "2"]
                                 [:varexpr
                                  [:int-literal "3"]]])
             => (eval/->stmt :invoke
                             (eval/->stmt :identifier "foo")
                             (list (eval/->stmt :literal 2)
                                   (eval/->stmt :varexpr
                                                (eval/->stmt :literal 3))))))

(facts "about invocation evaluation"
       (fact "evaluates invocations without arguments"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo"
                                                           (fn [args]
                                                             123))]))
                   [env* val] (eval/logos-eval env (eval/->stmt :invoke
                                                                (eval/->stmt :identifier "foo")
                                                                (list)))]
               env* => env
               val => 123))

       (fact "evaluates invocations with arguments"
             (let [env (-> (env/->empty-env)
                           (env/push-frame [(env/->binding "foo"
                                                           (fn [args]
                                                             (+ (first args)
                                                                (second args))))]))
                   [env* val] (eval/logos-eval env (eval/->stmt :invoke
                                                                (eval/->stmt :identifier "foo")
                                                                (list (eval/->stmt :literal 2)
                                                                      (eval/->stmt :literal 3))))]
               env* => env
               val => 5))
       
       (fact "evaluates lambda invocations"
             (let [env (-> (env/->empty-env)
                           (env/push-frame))
                   [env* val] (eval/logos-eval env (eval/->stmt :define
                                                                "foo"
                                                                (eval/->stmt :lambda
                                                                             (list [:arg "x"])
                                                                             [:identifier "x"])))
                   [env** val] (eval/logos-eval env* (eval/->stmt :invoke
                                                                  (eval/->stmt :identifier "foo")
                                                                  (list (eval/->stmt :literal 123))))]
               env** => env*
               val => 123))

       (fact "evaluates lambda invocations that refer to themselves with the special identifier 'self'"
             (let [env (-> (env/->empty-env)
                           (env/push-frame))
                   [env* val] (eval/logos-eval env (eval/->stmt :define
                                                                "foo"
                                                                (eval/->stmt :lambda
                                                                             (list)
                                                                             [:identifier "self"])))
                   [env** val] (eval/logos-eval env* (eval/->stmt :invoke
                                                                  (eval/->stmt :identifier "foo")
                                                                  (list)))]
               env** => env*
               val => fn?))

       (fact "evaluates nested lambda invocations"
             (let [env (-> (env/->empty-env)
                           (env/push-frame))
                   [env* val] (eval/logos-eval env (eval/->stmt :define
                                                                "foo"
                                                                (eval/->stmt :lambda
                                                                             (list [:arg "x"])
                                                                             (eval/->stmt :lambda
                                                                                          (list [:arg "y"])
                                                                                          [:identifier "x"]))))
                   [env** val] (eval/logos-eval env* (eval/->stmt :invoke
                                                                  (eval/->stmt :invoke
                                                                               (eval/->stmt :identifier "foo")
                                                                               (list (eval/->stmt :literal 2)))
                                                                  (list (eval/->stmt :literal 1))))]
               env** => env*
               val => 2)))
