![logo](logo.png)

# Logos prototype interpreter

## Introduction

This is a multi-platform interpreter for a prototype of Logos. Logos is a general-purpose, loosely typed, interpreted, functional programming language. It's being developed for no reason other than the author's fascination with the design and implementation of programming languages. As such, its nature is destined to be forever experimental rather than pragmatic. If you do happen to find some practical use in it: great! However, please note that the language and the interpreter are currently extremely susceptible to *radical* change.

## Setup

### Build dependencies

- [Leiningen](https://github.com/technomancy/leiningen)

### Building

Run the `build.sh` script.
It will produce a `build` directory, which is a self-contained directory with all the required files for Logos to work.
It will also create a redistributable archive with the contents of the build directory.

### Installation

1. Extract the archive to any installation directory using a tar utility (or use the build directory)
2. Add the installation directory to your `PATH`

## Usage

A launcher is provided for both Unix-like systems (the 'logos' file) and for Windows (the 'logos.bat' file). The usage instructions for Unix-like systems and Windows are the same.

The Logos interpreter has an interactive mode which starts a read-evaluate-print loop ("REPL" for short), which is like a very basic "shell". In this mode, the interpreter will prompt the user for input, evaluate it, and print the value of the input.
To start the interpreter in interactive mode, enter the following on your command line shell:
```bash
logos -i
```
Once you're in the REPL, you can quit using the REPL command `exit`. The `help` command will print a help message.
The REPL has a multi-line input buffer. When enter is pressed on an unfinished statement, it will be broken up and you can continue typing on the next line(s) until the statement is finished. To clear the buffer, use the `abort` command.

To run a Logos program non-interactively, you can also provide a path to a file that contains Logos code. To do that, enter the following:
```bash
logos -f /path/to/code.lgs
```

These two options can also be combined. In that case, first the file will be loaded into the interpreter, and when it's done, the REPL will be started in the context produced by loading the file. To do that, enter the following:
```bash
logos -i -f /path/to/code.lgs
```

You can try out all of the examples below in interactive or non-interactive mode; whichever you prefer. However, interactive mode is recommended, for a couple of reasons:
1. intermediate values can be directly inspected;
2. all the examples are formulated as if taken from an interactive session;
3. it's simpler to fiddle around and try your own variations on the examples.

When starting the REPL, an initialization script written in Logos will be automatically run. It's perfectly fine to customize this file, if you wish.

**NOTE:** the examples all presume that the core library is already loaded. If you're in a REPL session and you left the `repl-init.lgs` file untouched, this will be the case from the onset. Otherwise, enter:
```
@require core
```
You can read more about it below.

## The Logos language

### Syntax

A Logos program consists of a list of statements and directives, and can contain comments. A statement is a meaningful unit to the program, and will return a value or perform an action within the execution context. A directive is more like a command for the interpreter.

#### Comments

Comments can be inserted at any point using the '#' character. Everything from a '#' character to the end of the line is ignored by the interpreter. For example...
```
>>> 123 # here is an awesome number!
123
```

#### Directives

##### Requiring code

`@require` is a directive that can be used to load Logos code from a file. It can be used either by providing a path in your filesystem (absolute or relative), or a library spec. The former will load any file, the latter will load a unit from the Logos library. When using `@require`, code will not be loaded again if the same file or library was already loaded before.

To require code from a file, use:
```
@require "/path/to/code.lgs"
```

To require code from the Logos library, use:
```
@require group.subgroup.unit
```
For instance, to require the "string" unit from the "core" group in the library, use:
```
@require core.string
```

##### (Re)loading code

`@load` is a directive that will unconditionally load Logos code from a file. It's just like `@require`, but it will *always* load the code, regardless of whether it was already required or loaded before.

For instance, to load the "string" unit from the "core" group in the library, use:
```
@require core.string
```

**NOTE:** `@load` can be useful to hot-reload code that you're currently developing into the REPL. However, to just include some foreign code as a dependency, it's **strongly** recommended to use `@require` instead!

#### Statements

A Logos statement is an expression or a definition.

The general form of an expression statement is:
```
<expression>
```
For example...
```
>>> add(1, 2)
3
```
An expression statement is simply evaluated. However, its value will also become the value of the special expression '%'. More about that below, in the "Last value" section.

A definition binds the value of an expression to a name, and these names are lexically scoped. The value can then be referred to by using that name. The general form of a definition is:
```
<name> <- <expression>
```
For example...
```
>>> foo <- 123
*defined 'foo'*

>>> bar <- 456
*defined 'bar'*

>>> sub(bar, foo)
333
```

There is a special variation on the definition syntax to bind a lambda (see below for more on that) to a name. In most cases, you'll want to use this for function definitions. A lambda can be bound using the regular definition syntax too (because a lambda is a first-class value), but this form serves as syntactic sugar for it. The general form of it is this:
```
<name>(<argname>, <argname>, <...>) <- <body>
```
For example...
```
>>> double(n) <- add(n, n)
*defined 'double'*

>>> double(3)
6
```
... which is equivalent to...
```
>>> double <- (n) => add(n, n)
*defined 'double'*

>>> double(3)
6
```

A name can also be redefined if it's subsequently defined again within the same scope. If a name is redefined, then everything within the scope will refer to the new value, also in preceding function definitions. For example...
```
>>> foo <- 123
*defined 'foo'*

>>> double-foo() <- add(foo, foo)
*defined 'double-foo'*

>>> double-foo()
246

>>> foo <- 456
*redefined 'foo'*

>>> double-foo()
912
```

#### Expressions

An expression is a language unit that has a certain value. The evaluation of an expression is the computation of that value. Depending on its type, an expression can be simple or complex. Complex expressions are expressions that contain sub-expressions. The rest of this chapter deals with the various types of expressions.

#### Names

A name is an expression that refers to a binding. A binding can be a built-in primitive, a result of a definition, or an assignment of a value to an argument in a function. A name can consist of alphabetic characters (including capitals), digits and certain symbols. However, the first character cannot be a digit or a '-'. A name is lexically scoped. If a name occurs in a lambda body, its local scope is that body. If doesn't occur in a lambda, its local scope is the global scope.
For example...
```
>>> foo <- 123
*defined 'foo'*

>>> foo
123

>>> func(bar) <- add(foo, bar)
*defined 'func'*

>>> func(456)
579
```

#### Blocks

A block is an expression which consists of a list of zero or more statements. Its value is the value of the last statement in the block, or `nil` if it's the empty block. A block is mainly useful for assigning a multi-statement body to a lambda (see below). The general form is:
```
{ <statement> <statement> ... }
```

#### Last value

The "last value" expression enables you to refer to the value of the preceding statement, and can thus serve as a link to chain a list of statements together. It may be intuitive to understand this expression as meaning "it", or "that". For example...
```
>>> 123
123

>>> add(%, 5)
128

>>> div(%, 2)
64
```
... means "take 123, add 5 to it, then divide that by 2."
By the way, multiple statements don't necessarily have to be on separate lines of code; they just have to be whitespace-separated. So you can also write the preceding example as:
```
>>> 123 add(%, 5) div(%, 2)
64
```

#### Literals (and the various types)

Literals are expressions that represent a concrete value of a certain primitive type. They are self-evaluating expressions. Every primitive type in the language (except pairs; see `cons` below) has its own type of literal that produces a value from it. Some derivative types (which are represented in terms of other types) also have their own literals. All literals are simple expressions; however, a lambda has a body, which assumes the form of another expression. The various types of the language will be introduced together with their literals in this section. Lambdas, however, will be treated in a separate section.

##### Nil (and booleans)

Nil is a special type, which contains only one value, nil, the literal expression of which is:
```
nil
```
This value is significant to the language because it is produced under certain special circumstances other than evaluating the literal, and it's used as a branching value in conditionals (see below).

Logos has a boolean type, but it's not a primitive type; it's very thinly derived in the `core.boolean` library in terms of `nil` and a non-nil value; `true` is defined as 0, and `false` is defined as nil. Thus, it's by no means necessary to use `true` and `false`, because they are merely names. It's enough to use nil and non-nil values in predicates and conditionals.

##### Integers

Integer literals consist of digits. For example...
```
123
```
... produces a positive integer, 123.
Integers are signed, and can thus also be negative. To produce a negative integer, put a '-' sign in front of it. For example...
```
-123
```
... produces a negative integer, -123.

##### Decimals

Decimals are real numbers that consist of digits and a decimal point ('.'). For example...
```
12.34
```
... produces a positive decimal, 12.34.
And again, just like integers, decimals can be negative. For example...
```
-12.34
```
... produces a negative decimal, -12.34.

**NOTE:** the '-' sign is *not* an operator, but part of the syntax for numeric literals only. This means you can't negate the value of any expression by putting a '-' in front of it. Instead, you must use the function `negate` from `core.number`.

##### Keywords

Keywords are symbolic values. Their literal consists of a ':' character followed by a name. For example...
```
:foo
```
Their identity can be tested using the primitive `eq?` function. Two keywords that have the same name are the same, and two keywords that don't have the same name are not.

##### Strings

A string is a list of characters. In Logos, a string is a derivative type, but the syntax has a literal expression for them. It consists of any piece of text between two double quotes. For example...
```
"This is a string."
```
Except for the literal syntax, everything related to strings and their internal representation is in the `core.string` library.

#### Lambdas

##### Expressing a lambda

A lambda is an anonymous function. It consists of a list of argument names and a body. The body is any expression which may refer to the argument names or to other names within the lexical scope of the lambda. Its general form is...
```
{ |<argname>, <argname>, ...| <body> }
```
For example...
```
{ |a, b| add(mul(a, b), b) }
```
If you want the body to consist of multiple statements, use a block (see above). However, the use of a block is not necessary; the body can assume the form of any expression.
For example...
```
{ |n|
  m <- add(n, 5)
  mul(m, 2)
}
```
**NOTE:** a lambda body has its own local scope, but a block (on its own) does not. A block receives its local scope only if it's used as the body of a lambda.

##### Invoking a lambda

A lambda can be invoked by appending a list of values for its arguments to it. This will cause the names to be bound to those values in a new stack frame, and the expression in its body evaluated within the new context. The value of the invocation is then the value of the body expression. For example...
```
>>> foo(n) <- mul(n, 3)
*defined 'foo'*

>>> foo(2)
6
```

##### Self-reference

Loops are performed in Logos by way of recursion. A lambda can recur by referring to itself. Self-reference can be achieved by having a lambda refer to its own name, but it can also be achieved by referring to the special name `self`. Any lambda invocation will implicitly bind the name `self` to the lambda itself. Thus, any lambda body can refer to `self`, which enables anonymous functions to recur.

Here is an example of a recursive function (the base case and the recursive step are separated using a conditional; see below for more on that):
```
>>> factorial(n) <- [eq?(n, 0) : 1, else : mul(n, factorial(dec(n)))]
*defined 'factorial'*

>>> factorial(5)
120
```

And here's the same example, but without ever assigning a name to the function:
```
>>> { |n| [eq?(n, 0) : 1, else : mul(n, self(dec(n)))] }
[lambda]

%(5)
120
```

##### Lambdas as first-class values

Logos is a functional programming language, so a lambda is a first-class value like any other. They can be passed as arguments to other lambdas, returned by other lambdas, and bound to a name like any other value can. For example...
```
>>> zero?(n) <- eq?(n, 0)
*defined 'zero?'*

>>> complement(pred) <- { |n| not(pred(n)) }
*defined 'complement'*

>>> nonzero? <- complement(zero?)
*defined 'nonzero?'* 
```
This example defines a function `zero?` which tests if a given value equals 0. `complement` is a function which takes a predicate (a function whose value is boolean, like `zero?` is) and returns another function whose value is its boolean negation. It then defines a function `nonzero?`, which tests if a given value does not equal 0, in terms of the `complement` of `zero?`.

##### Closures

A lambda value carries an environment. Thus, a lambda "contains" the information in its environment, which can be "unpacked" by invoking it. This type of pattern is called a "closure". For example...
```
>>> container(x) <- { || x }
*defined 'container'*

>>> container(123)
[lambda]

>>> %()
123
```

##### Variadic functions

Lambdas can express variadic functions; that is, functions which may have a variable number of arguments rather than a fixed number. This can be achieved by using the "vararg" syntax. A vararg is a single argument name that 0 or more values can be bound to as a plain Logos list structure.
The general form (note that the "..." that tails the vararg is not metasyntactic in this case, but is literally the Logos syntax) is:
```
{ |<arg>, <arg>, <vararg>...| <body> }
```
For example...
```
>>> f <- { |a, b, rest...| rest }
*defined 'f'*

>>> f(1, 2, 3, 4, 5)
(3, 4, 5)
```
Or alternatively...
```
>>> f(a, b, rest...) <- rest
*defined 'f'*

>>> f(1, 2, 3, 4, 5)
(3, 4, 5)
```
**NOTE:** an argument list can contain only one vararg, and it can occur only at the end of the list.

Quite uniquely to Logos (as far as the author is aware), there is also a so-called "varexpr" syntax to do essentially the opposite thing as a vararg: provide a function *call* with an expression (presumably producing a Logos list) tailed by "..." at the end, which means that the Logos list is to be destructured such that its individual values are bound to the function arguments as though it were a list of arguments. This does not necessarily have to match an according vararg; the list can be bound to plain arguments as well, or even a combination of plain arguments and a vararg. This language feature has a number of possible use cases, but it's particularly useful in conjunction with varargs to define recursive variadic functions, as these functions will need to go back-and-forth between argument list and Logos list.
For example:
```
>>> f(a, b, c) <- b
*defined 'f'*

>>> f(list(:foo, :bar, :baz)...)
:bar
```
Or in conjunction with a vararg:
```
>>> f(a, rest...) <- rest
*defined 'f'*

>>> f(list(:foo, :bar, :baz)...)
(:bar, :baz)
```

#### Conditionals

Branching is performed using conditional expressions. A conditional expression consists of a list of 0 or more clauses. A clause contains two expressions: a condition on the left-hand side, and a consequent on the right-hand side. The evaluation of a conditional with the first clause in the list (if the list is empty, the conditional evaluates to nil). If the value of the condition is non-nil, it's satisfied and the consequent is evaluated, and the conditional evaluates to the value of the consequent. If the value of the condition is nil, it's not satisfied and the next clause is tried. If the end of the list is reached and no condition was satisfied, the conditional evaluates to nil.

The general forms of a conditional is:
```
[<condition>: <consequent>, <condition>: <consequent>, ...]
```
For example...
```
>>> [eq?(1, 1) : 123, eq?(1, 2) : 456]
123

>>> [eq?(1, 2) : 123, eq?(1, 1) : 456]
456

>>> [eq?(1, 2) : 123, eq?(3, 4) : 456]
nil
```
**NOTE:** the commas that separate the clauses are optional. Generally, if you want to write multiple clauses on a single line (aligning them horizontally), it's best to use a comma to visually distinguish them from each other. If you want to break clauses up into multiple lines (aligning them vertically), you may want to omit it.
So you could also write conditionals in this fashion:
```
>>> [eq?(1, 1) : 123
     eq?(1, 2) : 456]
123
```
**NOTE: ** the core library defines a name `else` as a non-nil value to provide some (pseudo-)syntactic sugar for final clauses that always hold when no other does. It's not at all necessary to use it; it can just make the code a bit more intuitive. For example...
```
[eq?(1, 3) : 123
 eq?(2, 3) : 456
 else      : 789]
```

### Primitive & core functions

This section is a guide to the primitive functions (built into the interpreter) and the core library. The core library can be loaded using:
```
@require core
```
You'll probably want to do this *in all cases*, as the core library contains a set of crucial functions and other values that are of general importance to any Logos program.
This manual will go over every primitive built into Logos, but not over every single thing from the core library. It will only provide a cursory glance on its modules. You are, however, strongly encouraged to read their source code (in the `lib/core` directory), because they contain a set of *extremely* useful and important tools, and they are all annotated with an informative description and an example usage.

#### Primitives

`nil`: The nil value.

`cons`: A function that creates a pair where the first argument is the left-hand value and the second argument is the right-hand value. The values of a pair can be anything, including another pair. This function is the only way to produce pairs, and pairs are the fundamental way to create relations. Therefore, this function is extremely important, because it's the fundamental basis of all derivative types and compound data structures.

`car`: A function that returns the left-hand value of a pair.

`cdr`: A function that returns the right-hand value of a pair.

`add`: A function that adds numbers.

`sub`: A function that subtracts numbers.

`mul`: A function that multiplies numbers.

`div`: A function that divides the first number by the rest (it always returns a decimal).

`mod`: A function that calculates the remainder of the first number divided by the second.

`pow`: A function that exponentiates two numbers. The first argument is the base, the second argument is the exponent.

`to-int`: A function that casts a number to an integer. Any decimal part will be cut off.

`eq?`: A function that tests if the given values are equal. It can be used to test keyword identity, numerical equivalence, and sameness of compound data structures built up in terms of `cons` pairs.

`lt?`: A function that tests if each given number is less than the next.

`rand`: A function that prays to the RNG gods for a uniformly distributed random decimal number between 0 and 1.

`print`: A function that prints a string to stdout.

`readln`: A function that reads a string from stdin.

#### `core.lang`

Adds some syntactic sugar to the Logos language.

#### `core.boolean`

Defines the `true` and `false` values, along with some useful boolean operations.

#### `core.list`

Defines the list data structure and its various operations. Lists are sequences of values which are internally represented in terms of a sequence of daisy-chained pairs, terminated by `nil`.

#### `core.table`

Defines the table data structure and its various methods. Tables are a data structure that contains a set of entries, each of which associate a key to a value, and the values can be looked up using their respective key. They are internally represented in terms of a list of key-value pairs.

#### `core.function`

Defines a set of useful higher-order functions.

#### `core.number`

Defines a set of useful numerical and aritmetical functions.

#### `core.string`

Defines the string type and its various operations. Strings are internally represented as lists of integers that are tagged with the :string keyword. Each integer in the list maps to an ASCII character.

#### `core.parsing`

Defines a set of functions to parse formatted strings to other types of values.

#### `core.formatting`

Defines a set of functions to format strings using other types of values.

#### `core.stdio`

Defines additional functions to interact with the standard i/o streams of the process.
